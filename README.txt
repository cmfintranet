CMFIntranet

  CMFIntranet is a Plone 2 based product for Zope. It is basically a fork
  of the CMFFhso project started at the FHSO Olten (www.fhso.ch). It's aim
  is to provide a powerful intranet solution.

Features

  * Extended workflow to satisfy the complex needs of an intranet solution

  * Modified skins

  * New Objects: CMFIdminPhone, CMFIFileTopic, CMFIProfile

  * Admin phone number handling

  * Photogallery of intranet members

  * Modified event management

Requires

  * Zope, till version 2.7.4 (see http://www.zope.org/Collectors/Zope/1779)

  * Plone 2.0.5

Installation

  * Install a "Plone Site" using the Zope Management Interface (ZMI) choosing "Plone Site"
    from the dropdown menu found under "Select type to add...".
    Choose "Create a new user folder in the portal".

  To install CMFIntranet:

  * Uncompress the CMFIntranet product into your zope/Products
    directory or link it there (Unix), e.g.:

    ln -s /path/to/installation /path/to/zope/Products

  * Use the 'portal_quickinstaller' to add the CMFIntranet Product.

  * Add a new User

    a. Go to "acl_users (Group-aware User Folder)"

    b. Click on the "Users" tab

    c. Enter your user names in the "Create users:" area.
       Users are seperated by a new line.

    d. Select the appropriate group in the "Affect groups" list.
       DON'T select a role if you are not 100% sure if this is needed!
       (The custom group to Plone role mapping is done automatically)

    e. You can set a default password for all the users you want to add.
       If you leave it empty, GRUF (Group User Folder) generates random ones.

    f. Press "Create"

Maintainers

  The following members are developing the product at the moment and can
  be reached by email for comments/questions/critics:

  * Reto Buerki, reto.buerki (at) fhso.ch (main developer)

Download

  No files released yet :)

Thanks

  Big thanks for my funky main logo go to:

  * demAltenHase, harry (at) bercom.net

