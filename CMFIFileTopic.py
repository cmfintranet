# -*- coding: UTF-8 -*-
# -*- Mode: Python; py-indent-offset: 4 -*-
# Authors:  Reto Buerki <reto.buerki (at) fhso.ch> 
#
# $Id$
# $HeadURL$

import os, urllib, string
from Globals import InitializeClass
from AccessControl import ClassSecurityInfo, getSecurityManager
from Products.CMFDefault.DublinCore import DefaultDublinCoreImpl
from Products.CMFDefault.Document import Document
from Products.CMFCore.WorkflowCore import WorkflowAction

# Import permission names
from Products.CMFCore import CMFCorePermissions

factory_type_information = (
    {'id': 'CMFIFileTopic',
     'meta_type': 'CMF CMFIFileTopic',
     'icon': 'file_icon.gif',
     'description': ('CMFI Files to include into a CMF Site.'),
     'product': 'CMFIntranet',
     'factory': 'addCMFIFileTopic',
     'immediate_view': 'cmfi_filetopic_edit_form',
     'actions': ({'id': 'view',
                  'name': 'View',
                  'action': 'cmfi_filetopic_view',
                  'permissions':  (CMFCorePermissions.View,)},
                 {'id': 'edit',
                  'name': 'Edit',
                  'action': 'portal_form/cmfi_filetopic_edit_form',
                  'permissions': (CMFCorePermissions.ModifyPortalContent,)},
                 {'id': 'metadata',
                  'name': 'Properties',
                  'action': 'portal_form/metadata_edit_form',
                  'permissions': (CMFCorePermissions.ModifyPortalContent,)},
                 ),
     },
    )

def addCMFIFileTopic(self
                    , id
                    , title = ''
                    , description = ''
                    , text = ''
                    , text_format = ''
                    , subjects = None
                    , REQUEST=None):
    """Create an empty CMFIFileTopic"""
    cmfi_files_object = CMFIFileTopic(id,title,description,text,text_format)
    self._setObject(id, cmfi_files_object)

class CMFIFileTopic(Document):
    """A CMFIFileTopic Item"""

    meta_type='CMF CMFIFileTopic'
    
    __implements__ = Document.__implements__ #redundant, but explicit
    
    security = ClassSecurityInfo()
    security.declareObjectPublic()
    def __init__(self
                 , id
                 , title=''
                 , description=''
                 , text=''
                 , subjects=[]):
        DefaultDublinCoreImpl.__init__(self)
        self.id = id
        self.title = title
        self.description = description
        self.text = text
        self.subjects = self.wrapType(subjects)

    def wrapType(self, thisType):
        if type(thisType) != type([]):
            return [thisType]
        else:
            return thisType

    security.declarePublic('getSubjects')
    def getSubjects(self):
        result = []
        for subject in self.subjects:
	    subject = subject[subject.find('/')+1:len(subject)]
            result.append(subject)
        # append also the context (e.g. research)	
        meta_subjects = self.Subject()
        for m in meta_subjects:
            result.append(m)
            
        return result

    security.declarePublic('getMyItems')
    def getMyItems(self):
        # we cannot only rely on portal_catalog here,
	# because the Subject_operator 'and' is not really 'and'
	# ;(
        my_subjects = self.getSubjects()
        items = self.portal_catalog.searchResults(sort_on='modified'
                                    , sort_order='reverse'
                                    , Subject = my_subjects
                                    , Subject_operator = 'and'
                                    , review_state = ('public','publicintern'))
        # double check each item if it's really korrekt        
        results = []
        my_subjects.sort()
	ok = 1
        for item in items:
            obj = item.getObject()
            meta_subjects = list(obj.Subject())
	    meta_subjects.sort() 
            for s in my_subjects:
                if s not in meta_subjects:
                    ok = 0            
            if ok:
	        results.append(obj)
        return results
    
    security.declareProtected(CMFCorePermissions.ModifyPortalContent, 'edit')
    def edit(self
             , title=''
             , description=''
             , text=''
             , text_format=None
             , subjects=[]):
        """Change the instance's properties """
        self.title = title
        self.subjects = self.wrapType(subjects)
        if text_format is None:
            text_format = getattr(self, 'text_format', 'html')
        if description is not None:
            self.setDescription(description)
        Document.edit(self,text_format,text)
           
    edit = WorkflowAction(edit) 
    

    ### Content accessor methods
    security.declareProtected(CMFCorePermissions.View, 'SearchableText')
    def SearchableText(self):
        """ Used by the catalog for basic full text indexing """
        return "%s %s %s" % ( self.title
                            , self.description
                            , self.text
                            )
    
InitializeClass(CMFIFileTopic)
    
