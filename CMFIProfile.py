# -*- coding: UTF-8 -*-
# -*- Mode: Python; py-indent-offset: 4 -*-
# Authors:  Reto Buerki <reto.buerki (at) fhso.ch>
#
# $Id$
# $HeadURL$

import os, urllib, string
from Globals import InitializeClass
from AccessControl import ClassSecurityInfo, getSecurityManager
from Products.CMFDefault.DublinCore import DefaultDublinCoreImpl
from Products.CMFDefault.Document import Document
from Products.CMFCore.WorkflowCore import WorkflowAction

# Import permission names
from Products.CMFCore import CMFCorePermissions

factory_type_information = (
    {'id': 'CMFIProfile',
     'meta_type': 'CMF CMFIProfile',
     'icon': 'user.gif',
     'description': ('CMFI Profile to include into a CMF Site.'),
     'product': 'CMFIntranet',
     'factory': 'addCMFIProfile',
     'immediate_view': 'cmfi_profile_edit_form',
     'actions': ({'id': 'view',
                  'name': 'View',
                  'action': 'cmfi_profile_view',
                  'permissions':  (CMFCorePermissions.View,)},
                 {'id': 'edit',
                  'name': 'Edit',
                  'action': 'portal_form/cmfi_profile_edit_form',
                  'permissions': (CMFCorePermissions.ModifyPortalContent,)},
                 {'id': 'metadata',
                  'name': 'properties',
                  'action': 'portal_form/metadata_edit_form',
                  'permissions': (CMFCorePermissions.ModifyPortalContent,),
                  'visible' : 0},
                 ),
     },
    )

def addCMFIProfile(self
                   , id
                   , title = ''
                   , userid = None
                   , salutation = ''
                   , token = ''
                   , acad_title = ''
                   , public_services = None
                   , services_centraloffice = None
                   , categories = None
                   , role = ''
                   , firstname = ''
                   , name = ''
                   , divisions = None
                   , address = ''
                   , telDirect = ''
                   , telFax = ''
                   , email = ''
                   , roomnr = ''
                   , mobile = ''
                   , entryDate = ''
                   , publications = ''
                   , text = ''
                   , text_format = ''
                   , REQUEST=None):
    """Create an empty CMFIProfile"""
    cmfi_profile_object = CMFIProfile(id,userid,title,salutation,firstname,name,token,entryDate,divisions,
                                      address, roomnr, telDirect, telFax, email, mobile,
                                      categories, role, acad_title, public_services, services_centraloffice,
                                      publications,text,text_format)
    self._setObject(id, cmfi_profile_object)

class CMFIProfile(Document):
    """A Profile Item"""

    meta_type='CMF CMFIProfile'
    
    __implements__ = Document.__implements__ #redundant, but explicit
    
    security = ClassSecurityInfo()
    security.declareObjectPublic()
    def __init__(self
                 , id
                 , title=''             # title of the Profile within the system
                 , userid=None          # the unique userid 
                 , salutation=''
                 , acad_title=''
                 , firstname=''
                 , name=''
                 , token=''
                 , divisions=[]
                 , address=''
                 , telDirect=''
                 , telFax=''
                 , mobile=''
                 , email=''
                 , roomnr=''
                 , entryDate=''
                 , categories=[]
                 , role=''
                 , public_services=[]
		 , services_centraloffice=[]
                 , text=''
                 , publications=''
                 , text_format='html'):
        """Initialize an instance of the class"""
        DefaultDublinCoreImpl.__init__(self)
        self.id = id
        self.title = title
        self.userid = userid
        self.salutation = salutation
        self.acad_title = acad_title
        self.firstname = firstname
        self.name = name
        self.token = token
        self.divisions = self.wrapType(divisions)
        self.address = address
        self.telDirect = telDirect
        self.telFax = telFax
        self.mobile = mobile
        self.email = email
        self.roomnr = roomnr
        self.entryDate = entryDate
        self.categories = self.wrapType(categories)
        self.role = role
        self.public_services = self.wrapType(public_services)
        self.services_centraloffice = self.wrapType(services_centraloffice)
        self.publiations = publications
        self.text = text
        self.tex_format = text_format
        
    def wrapType(self, thisType):
        if type(thisType) != type([]):
            return [thisType]
        else:
            return thisType
        
    #security.declarePublic('titleToString')
    #def titleToString(self):
        #title = ""
        #for tmpStr in self.acad_title:
            #title = string.join([title, tmpStr])
        #title = string.join([title, self.acad_title2])
        #return title

    security.declarePublic('formatTelNumberWithCountryCode')
    def formatTelNumberWithCountryCode(self, telNr):
        '''format given telephone number to a format like +41 62 3882 541'''
        tel = self.portal_cmfi_tool.getItemFromGP('cmfi_country_code')
        telNr = string.strip(string.replace(telNr,' ',''))
        tel = tel + ' ' + telNr[1:3] + ' ' + telNr[3:7] + ' ' + telNr[7:] 
        return tel
    
    security.declarePublic('formatTelNumber')
    def formatTelNumber(self, telNr):
        '''format given telephone number to a format like 062 3882 541'''
        telNr = string.strip(string.replace(telNr,' ',''))
        tel = telNr[:3] + ' ' + telNr[3:7] + ' ' + telNr[7:] 
        return tel
    
    security.declareProtected(CMFCorePermissions.ModifyPortalContent, 'edit')
    def edit(self
             , title
             , userid
             , salutation
             , firstname
             , name
             , token
             , address
             , email
             , telDirect
             , roomnr
             , mobile
             , entryDate
             , acad_title
             , telFax
             , divisions=[]
             , categories=[]
             , role=''
             , public_services=[]
             , services_centraloffice=[]
             , text=''
             , publications=''
             , text_format=None):
        """Change the instance's properties """
        self.title = title
        self.userid = userid
        self.acad_title = acad_title
        self.salutation = salutation
        self.firstname = firstname
        self.name = name
        self.token = token
        self.divisions = self.wrapType(divisions)
        self.address = address
        self.telDirect = telDirect
        self.telFax = telFax
        self.mobile = mobile
        self.roomnr = roomnr
        self.entryDate = entryDate
        self.telCompany = self.portal_cmfi_tool.getItemFromGP('cmfi_tel_company')
        user = self.portal_membership.getAuthenticatedMember().id
        if len(email) == 0:
           self.email = self.portal_cmfi_tool.getEmailFromMemberdata(userid)
        # check if authenticated user is owner of this profile. Only the owner can override the email in
        # the preferences
        elif user == self.userid:           
            self.email = email
            self.portal_cmfi_tool.setEmailForMember(userid, email)
        self.categories = self.wrapType(categories)
        self.role = role
        self.public_services = self.wrapType(public_services)
        self.services_centraloffice = self.wrapType(services_centraloffice)
        self.publiations = publications
        if text_format is None:
            text_format = getattr(self, 'text_format', 'html')
        Document.edit(self,text_format,text)
        
        self.reindexObject()
           
    edit = WorkflowAction(edit) 

    ### Content accessor methods
    security.declareProtected(CMFCorePermissions.View, 'SearchableText')
    def SearchableText(self):
        """ Used by the catalog for basic full text indexing """
        return "%s %s %s %s %s %s %s %s %s %s %s" % ( self.title
                                                    , self.divisions
                                                    , self.name
                                                    , self.firstname
                                                    , self.token
                                                    , self.description
                                                    , self.text
                                                    , self.address
                                                    , self.roomnr
                                                    , self.categories
                                                    , self.services_centraloffice
                                             )
    
InitializeClass(CMFIProfile)
    
