# -*- coding: UTF-8 -*-
# -*- Mode: Python; py-indent-offset: 4 -*-
# Authors: <reto.buerki (at) fhso.ch>
#
# $Id$
# $HeadURL$

from Globals import InitializeClass
from AccessControl import ClassSecurityInfo
from Products.CMFCore.utils import UniqueObject
from OFS.SimpleItem import SimpleItem
from Products.CMFCore.utils import getToolByName
from OFS.Image import manage_addImage
import os

# Import permission names
from Products.CMFCore import CMFCorePermissions

class ConfigTool(UniqueObject, SimpleItem):
    """ 
    Class to handle all CMFIntranet configs
    """
    id = 'portal_cmfi_config_tool'
    meta_type= 'CMFI Config Tool'
    # where are we?
    INSTANCE = os.environ.get('INSTANCE_HOME', '')

    security = ClassSecurityInfo()       	    
    
    security.declareProtected(CMFCorePermissions.ModifyPortalContent, 'delete_global_prop_items')
    def delete_global_prop_items(self, proplist, prop_name):
        """
        Delete items from a global property
        """
        props = self.portal_cmfi_tool.getItemFromGP(prop_name)
	new_props = []
        for prop in props:
            if not prop in proplist:
                new_props.append(prop)
        global_properties = self.portal_properties.cmfi_properties
        global_properties.manage_changeProperties({prop_name:new_props})
        return 1

    security.declareProtected(CMFCorePermissions.ModifyPortalContent, 'add_global_prop_items')
    def add_global_prop_items(self, prop, prop_name):
        """
        Add items to global Property
        """
        # get properties first
        props = self.portal_cmfi_tool.getItemFromGP(prop_name)
	props=list(props)
        # append the new ones
        props.append(prop)
        global_properties = self.portal_properties.cmfi_properties
        global_properties.manage_changeProperties({prop_name:props})
        return 1

    security.declareProtected(CMFCorePermissions.ModifyPortalContent, 'get_conf_cmfiprops')
    def get_conf_cmfiprops(self):
        """
        Returns all cmfi global properties for configuration
	except some which are not needed for intranet config
        """
        exclude_list = ('title',
                        'cmfi_cmfiprofile_reviewer',
                        'cmfi_processesBox',
                        'cmfi_file_types',
                        'cmfi_site_layout',
			'cmfi_portlet_telephone',
			'cmfi_portlet_gallery',
                        'cmfi_portlets_available')
        all_props = self.portal_properties.cmfi_properties.propertyItems();
        act_props = []
        for prop in all_props:
            if not prop[0] in exclude_list:
                act_props.append(prop)
        return act_props		

    security.declareProtected(CMFCorePermissions.ModifyPortalContent, 'get_cmfi_props_string')
    def get_cmfi_props_type(self, type):
        """
        Returns all cmfi props with type 'type'
	"""
	prop_object = self.portal_properties.cmfi_properties
        all_props = self.get_conf_cmfiprops()
	act_props = []
 
        for prop in all_props:
            if prop_object.getPropertyType(prop[0]) == type:
                act_props.append(prop)
        return act_props	

    security.declareProtected(CMFCorePermissions.ModifyPortalContent, 'add_main_folder')
    def add_main_folder(self, folderstruct):
        """ 
        Adds a new main folder to the portal:
        - creates folder if it does not exists
        - creates "confidental" subfolder
        - registers portlets
        - register new portal_tab action
	- creates default view for process
	-> calls function to add user and groups
        """
        (title, foldername) = folderstruct.split('/', 1)
	
        portal = getToolByName(self, 'portal_url').getPortalObject()

        folder = getattr(portal, foldername, None)
        # create folder 
        if not folder:
            portal.invokeFactory('Folder', foldername)
            folder = getattr(portal, foldername)
        folder.manage_changeProperties(REQUEST={'title':title})
        folder.manage_permission('View', ['Authenticated'], 1)
        folder.manage_permission('Access contents information', ['Authenticated'], 1)
        folder.manage_permission('List folder contents', ['Member', 'Manager','Reviewer'], 0)
	
        copy_html  = portal.portal_skins.custom.manage_copyObjects(('index_html_proc'))
        folder.manage_pasteObjects(copy_html)
        folder.manage_renameObjects(['index_html_proc'],['index_html'])
                
	cfolder = getattr(folder, 'confidential', None)
        if not cfolder:
            folder.invokeFactory('Folder', 'confidential')
            cfolder = getattr(folder, 'confidential')
            cfolder.manage_permission('View', ['Authenticated','Manager','Owner','Reviewer'], 0)
            cfolder.manage_permission('Access contents information', ['Authenticated','Manager','Owner','Reviewer'], 0)
            cfolder.manage_permission('List folder contents', ['Manager','Reviewer'], 0)
        cfolder.manage_changeProperties(REQUEST={'title':'Vertraulich'})

        if folder.hasProperty('left_slots'):   
            folder.manage_delProperties(['left_slots',])
        left_slots=('here/portlet_navigation/macros/portlet'
                    , 'here/portlet_news/macros/portlet'
		    , 'here/portlet_slinks/macros/portlet'
                    , 'here/portlet_processes/macros/portlet'
                    , 'here/portlet_files/macros/portlet')
        folder.manage_addProperty('left_slots', left_slots, 'lines') 
        # register as portal_tab
        mt=getToolByName(self, 'portal_actions')  
        mt.addAction(foldername
		    ,title
                    ,'string: $portal_url/' + foldername
                    ,''
                    ,'View'
                    ,'portal_tabs'
                    , visible=1)
        # register groups
	if not self.register_folder_groups(foldername, cfolder, folder):
            return 0
	return 1

    security.declareProtected(CMFCorePermissions.ModifyPortalContent, 'del_main_folder')
    def del_main_folder(self, folderstruct):
        """
	Deletes a main folder:
        - delete folder including subfolders
        - delete "confidental" folder
        - calls unregister_folder_groups to delete groups
        """
        (title, foldername) = folderstruct.split('/', 1)
       	
        portal = getToolByName(self, 'portal_url').getPortalObject()

	try:
            # if this fails, we assume that the folder is already gone
	    portal.manage_delObjects(foldername)
        except:
            pass

        # unregister portal_tab
        try:
            # if this fails, we assume that the action is already gone
            mt=getToolByName(self, 'portal_actions')  
            if foldername in [action.id for action in mt.listActions()]:
                listA = mt.listActions()
                selections = tuple([i for i in range(0,len(listA)) if listA[i].id==foldername])
                mt.deleteActions(selections)
        except:
            pass

        # unregister groups
        self.unregister_folder_groups(foldername)
        
        # for now, we always return true 
        return 1

    security.declareProtected(CMFCorePermissions.ModifyPortalContent, 'register_folder_groups')
    def register_folder_groups(self, foldername, cfolder, folder):
        """
        Registers all the needed groups of a folder
        """
        groupsuffixes = [                    
                    ('_member', ['Member'], '', 0, '', '') ,
                    ('_reviewer',['Reviewer'], ['Reviewer'], 0, ['Add portal content'], 'cms-home'),
                    ('_manager', ['Manager'], ['Manager'], 1, '', '')
                    ]	
        portal = getToolByName(self, 'portal_url').getPortalObject()
        acl_users = getattr(portal, 'acl_users')
        user = acl_users.getUser(foldername)
        if not user:
            acl_users.userFolderAddUser(foldername, foldername, [], [])
        for suffix, roles, toportal, toconfidential, targetpermission, commonfolders in groupsuffixes:
            thisgroupname = foldername + suffix
            try:
                acl_users.userFolderAddGroup(foldername + suffix, roles)
            except:
                pass            
            # add groups and roles for confidential
            if toconfidential:                
	        # does the role need a special permission in the confidential folder
                # yes, if role wants to copy
                if len(targetpermission):
                    for role in roles:
                        pofrole = []
                        for r in cfolder.permissionsOfRole(role):
                            if r['selected'] == 'SELECTED':
                                pofrole.append(r['name'])
                        cfolder.manage_role(role,pofrole + targetpermission)
	    else:
                # does the role need a special permission in the target folder
                # yes, if role wants to copy
                if len(targetpermission):
                    for role in roles:
                        pofrole = []
                        for r in folder.permissionsOfRole(role):
                            if r['selected'] == 'SELECTED':
                                pofrole.append(r['name'])
                        folder.manage_role(role, pofrole + targetpermission)
            
            # do we have to deal with common folders where every reviewer can publish
            if len(commonfolders):
                common = portal
                for part in commonfolders.split('/'):
                    try:
                        common = getattr(common,part)
                    except:
                        raise  
        return 1      

    security.declareProtected(CMFCorePermissions.ModifyPortalContent, 'unregister_folder_groups')
    def unregister_folder_groups(self, foldername):
        """
        Delete groups
        """
        # simple form
        groupsuffixes = ('_reviewer',
			 '_manager',
			 '_member')
	
        portal = getToolByName(self, 'portal_url').getPortalObject()
        acl_users = getattr(portal, 'acl_users')
        prefix = acl_users.getGroupPrefix()

        del_groups = []
	for p in groupsuffixes:
            del_groups.append(prefix + foldername + p)

        acl_users.userFolderDelGroups(del_groups)  
	acl_users.userFolderDelUsers([foldername,])
			
    security.declareProtected(CMFCorePermissions.ModifyPortalContent, 'add_main_struct')
    def add_main_struct(self, struct):
        """
        Add a new main site structure
        """
	
	if not self.add_main_folder(struct):
            return 0
	    
        # add new site_layout entry
        props = self.portal_cmfi_tool.getItemFromGP('cmfi_site_layout')
        props = list(props)
	props.append(struct + ":")
	props = tuple(props)
        global_properties = self.portal_properties.cmfi_properties
        global_properties.manage_changeProperties({'cmfi_site_layout':props})   

	return 1

    security.declareProtected(CMFCorePermissions.ModifyPortalContent, 'del_main_struct')
    def del_main_struct(self, struct):
        """
        Delete a main site structure
        """  
        
        if not self.del_main_folder(struct):
            return 0

        # delete site_layout entry
       	props = self.portal_cmfi_tool.getItemFromGP('cmfi_site_layout')
	new_props = []
        for p in props:
            if not p.find(struct + ':') == 0:
                new_props.append(p)    
	
	global_properties = self.portal_properties.cmfi_properties
	global_properties.manage_changeProperties({'cmfi_site_layout':new_props}) 

	return 1
           
	
InitializeClass(ConfigTool)

