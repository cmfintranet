########################################################################
#
# Calendar Tool by Andy Dawkins (New Information Paradigms Ltd)
#
# Converted to a CMF Tool by Alan Runyan
#
# Additional Modification for the CMFCalendar by Andy Dawkins 29/04/2002
#
########################################################################


import calendar
calendar.setfirstweekday(6) #start day  Mon(0)-Sun(6)
from DateTime import DateTime

#from Products.CMFCore.utils import UniqueObject
#from Products.CMFCore.utils import _checkPermission, _getAuthenticatedUser
#from Products.CMFCore.utils import getToolByName, _dtmldir
#from OFS.SimpleItem import SimpleItem
#from Globals import InitializeClass
from AccessControl import ClassSecurityInfo
#from Products.CMFCore.CMFCorePermissions import ManagePortal
#from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from Products.CMFCalendar.CalendarTool import CalendarTool as BaseTool

class CalendarTool (BaseTool):
    """ a calendar tool for encapsualting how calendars work and are displayed """
    meta_type= 'CMFI Calendar Tool'
    security = ClassSecurityInfo()
    cmfi_tool = 1

    security.declarePublic('catalog_getevents')
    def catalog_getevents(self, year, month):
        """ given a year and month return a list of days that have events """
        first_date=DateTime(str(month)+'/1/'+str(year))
        last_day=calendar.monthrange(year, month)[1]
        ## This line was cropping the last day of the month out of the
        ## calendar when doing the query
        ## last_date=DateTime(str(month)+'/'+str(last_day)+'/'+str(year))
        last_date=first_date + last_day    
        
        query=self.portal_catalog(Type=self.calendar_types,
                                  review_state=['public','published'],	                          
                                  start=(first_date, last_date),
                                  start_usage='range:min:max',
                                  sort_on='start')
        # I don't like doing two searches
        # What i want to do is
        # start date => 1/1/2002 and start date <= 31/1/2002
        # or
        # end date => 1/1/2002 and end date <= 31/1/2002
        # but I don't know how to do that in one search query :(  - AD

        # if you look at calendar_slot you can see how to do this in 1 query - runyaga
        query+=self.portal_catalog(Type=self.calendar_types,
                                   review_state=['public','published'],
                                   end=(first_date, last_date),
                                   end_usage='range:min:max',
                                   sort_on='end')
        
        # compile a list of the days that have events
        eventDays={}
        for daynumber in range(1, 32): # 1 to 31
            eventDays[daynumber] = {'eventslist':[], 'event':0, 'day':daynumber}
        includedevents = []
        for result in query:
            if result.getRID() in includedevents:
                break
            else:
                includedevents.append(result.getRID())
            event={}
            # we need to deal with events that end next month
            if  result.end.month() != month:  # doesn't work for events that last ~12 months - fix it if it's a problem, otherwise ignore
                eventEndDay = last_day
                event['end'] = None
            else:
                eventEndDay = result.end.day()
                event['end'] = result.end.Time()
            # and events that started last month
            if result.start.month() != month:  # same as above re: 12 month thing
                eventStartDay = 1
                event['start'] = None
            else:
                eventStartDay = result.start.day()
                event['start'] = result.start.Time()
            event['title'] = result.Title or result.id
            if eventStartDay != eventEndDay:
                allEventDays = range(eventStartDay, eventEndDay+1)
                eventDays[eventStartDay]['eventslist'].append({'end':None, 'start':result.start.Time(), 'title':result.Title})
                eventDays[eventStartDay]['event'] = 1
                for eventday in allEventDays[1:-1]:
                    eventDays[eventday]['eventslist'].append({'end':None, 'start':None, 'title':result.Title})
                    eventDays[eventday]['event'] = 1
                eventDays[eventEndDay]['eventslist'].append({'end':result.end.Time(), 'start':None, 'title':result.Title})
                eventDays[eventEndDay]['event'] = 1
            else:
                eventDays[eventStartDay]['eventslist'].append(event)
                eventDays[eventStartDay]['event'] = 1
            # This list is not uniqued and isn't sorted
            # uniquing and sorting only wastes time
            # and in this example we don't need to because
            # later we are going to do an 'if 2 in eventDays'
            # so the order is not important.
            # example:  [23, 28, 29, 30, 31, 23]
        return eventDays

    security.declarePublic('getEventsForThisDay')
    def getEventsForThisDay(self, thisDay):
        """ given an exact day return ALL events that:
            A) Start on this day  OR
            B) End on this day  OR
            C) Start before this day  AND  end after this day"""
        
        catalog = self.portal_catalog
        
        first_date, last_date = self.getBeginAndEndTimes(thisDay.day(), thisDay.month(), thisDay.year())
        #first_date=DateTime(thisDay.Date()+" 00:00:00")
        #last_date=DateTime(thisDay.Date()+" 23:59:59")

        # Get all events that Start on this day
        query=self.portal_catalog(Type=self.calendar_types,
                                  review_state=['public','published'],	                          
                                  start=(first_date,last_date),
                                  start_usage='range:min:max')
        
        # Get all events that End on this day
        query+=self.portal_catalog(Type=self.calendar_types,
                                  review_state=['public','published'],	                          
                                  end=(first_date,last_date),
                                  end_usage='range:min:max')

        # Get all events that Start before this day AND End after this day
        query+=self.portal_catalog(Type=self.calendar_types,
                                  review_state=['public','published'],
                                  start=first_date,
                                  start_usage='range:max',
                                  end=last_date,
                                  end_usage='range:min')

        # Unique the results
        results = []
        rids = []
        for item in query:
            rid = item.getRID()
            if not rid in rids:
                results.append(item)
                rids.append(rid)
                
        def sort_function(x,y):
            z = cmp(x.start,y.start)
            if not z: 
                return cmp(x.end,y.end)
            return z
        
        # Sort by start date
        results.sort(sort_function)
                
        return results
