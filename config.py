# -*- coding: UTF-8 -*-
# -*- Mode: Python; py-indent-offset: 4 -*-
# Authors: Reto Buerki <reto.buerki (at) cmfi.ch> 
#
# $Id$
# $HeadURL$

from Globals import package_home

GLOBALS = globals()
PROJECTNAME = "CMFIntranet"

cmfi_configlet_props = {
    'id': 'CMFIntranet_properties',
    'appId': PROJECTNAME,
    'name': 'CMFIntranet Properties',
    'action': 'string:$portal_url/prefs_cmfi_props',
    'category': 'Products',
    'permission': ('Manage portal',),
    'imageUrl': 'cmfi_icon.gif',
    }

cmfi_configlet_mngt = {
    'id': 'CMFIntranet_management',
    'appId': PROJECTNAME,
    'name': 'CMFIntranet Management',
    'action': 'string:$portal_url/prefs_cmfi_management',
    'category': 'Products',
    'permission': ('Manage portal',),
    'imageUrl': 'cmfi_icon.gif',
    }


cmfi_properties = (
    ('cmfi_public_services', 'lines', 
     ['Ausbildung',
      'Weiterbildung',
      'Dienstleistung',
      'Forschung']),
    ('cmfi_female_salutation', 'string', 'Frau'),
    ('cmfi_businesscard_service_email', 'string', 'bizniz.cards@cmfi.ch'),
    ('cmfi_categories', 'lines', 
     ['Mitarbeiter/Mitarbeiterin',
      'Direktorin/Direktor',
      'Praktikantin/Praktikant']),
    ('cmfi_titel', 'string', 'Company Name'),
    ('cmfi_site_layout', 'lines', 
     ['Dienste/centraloffice: \
         Personal/centraloffice.hr_management, \
	 Rechnungen/centraloffice.finance, \
	 IT Management/centraloffice.it_management, \
	 CCC/centraloffice.ccc']),
    ('cmfi_country_code', 'string', '+41'),    
    ('cmfi_file_types', 'lines', 
     ['Checklisten:Checkliste/checklist',
      'Formulare:Formular/form',
      'Newsletter:Newsletter/newsletter',
      'Reglemente:Reglement/regulation',
      'Vorlagen:Vorlage/template',
      'Vertr�ge:Vertrag/agreement',
      'Weisungen:Weisung/instruction',
      'Allgemeines:Allgemein/general',
      'Folien:Folien/transparency_film',
      'Protokolle:Protokolle/protocol',
      'Pressespiegel:Pressespiegel/press_review',
      'Planungshilfen:Planungshilfen/planning_aids',
      'Jahresberichte:Jahresbericht/annual_report',
      'Forschungsberichte:Forschungsbericht/research_report',
      'Wording:Wording/wording']),
    ('cmfi_tel_company', 'string', '+41 (0)848 821 011'),
    ('cmfi_salutations', 'lines', ['Frau', 'Herr']),   
    ('cmfi_divisions', 'lines', 
     ['Rat',
      'Direktion',
      'Zentrale Dienste',
      'Gewerkschaften']),
    ('cmfi_url', 'string', 'www.cmfi.ch'),
    ('cmfi_main_addr', 'lines', ['Sportstrasse 2', 'CH-2540 Grenchen']),
    ('cmfi_address', 'lines', 
     ['Bern Fakestreet',
      'Olten Foostreet']),
    ('cmfi_cmfiprofile_reviewer', 'lines', 
     ['centraloffice.hr_management_reviewer',
      'centraloffice.hr_management_manager']), 
    ('cmfi_cmfihome_reviewer', 'lines', 
     ['cms-home_reviewer']),
    ('cmfi_floors', 'lines', ['2','3','4']),
    ('cmfi_portlets_available', 'lines',
     ['portlet_gallery',
      'portlet_telephone']),
    ('cmfi_portlet_telephone', 'string', 'cmfi_divisions'),
    ('cmfi_portlet_gallery', 'string', 'cmfi_categories'),
    ('cmfi_check_mail', 'int', 1),
    ('cmfi_portlet_order', 'lines',
     ['cmfi_address',
      'cmfi_divisions',
      'cmfi_categories',
      'cmfi_public_services']),
    ('cmfi_portlet_round', 'int', 0),
    )


