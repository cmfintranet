# -*- coding: UTF-8 -*-
# -*- Mode: Python; py-indent-offset: 4 -*-
# Authors:  Reto Buerki <reto.buerki (at) fhso.ch>
#
# $Id$
# $HeadURL$

from Products.CMFCore.utils import UniqueObject
from OFS.SimpleItem import SimpleItem
from AccessControl import ClassSecurityInfo
from Globals import InitializeClass
from Products.CMFCore.utils import getToolByName
from urllib import quote_plus

class CMFITool (UniqueObject, SimpleItem):
    """ 
    Provides some utilities to manage cmfi portal content 
    """
    id = 'portal_cmfi_tool'
    meta_type= 'CMFI Tool'
    security = ClassSecurityInfo()
    cmfi_tool = 1

#####################################################################################################
# ** WRAPPER function ***
# -> functions is used to access information stored in global portal_properties
#####################################################################################################
    
    security.declarePublic('getItemFromGP')
    def getItemFromGP(self, item):
        """
	** WRAPPER function to access all cmfi properties **
	Returns an item (line, string, boolean)
	from global cmfi properties
	"""
	item = getattr(self.portal_properties.cmfi_properties, item)
	return item
    
#####################################################################################################
# Site layout functions
# -> functions operate on getSiteLayout()
#####################################################################################################
	
    security.declarePublic('getProcesses')
    def getProcesses(self):
        """
	Returns the defined processes
	"""
        siteLayout = self.getSiteLayout();
        return siteLayout.keys()    
	
    security.declarePublic('getSiteLayoutTreeAsInPortalActions')
    def getSiteLayoutTreeAsInPortalActions(self, simple=None):
	"""
	Return site layout tree as it is in Portal actions
	"""
        rows = self.getSiteLayoutTree(simple)
        tree = []
        at = getToolByName(self, 'portal_actions')
        for action in at._cloneActions():
            if action.category == 'portal_tabs' and action.visible:
                subFlag = 0
                for row in rows:
                    if subFlag == 1 and row[0] == 1:
                        tree.append(row)
                        subFlag = row[0]
                    elif subFlag == 1:
                        break
                    elif action.id == row[1]:
                        tree.append(row)
                        subFlag = 1
        return tree
        
    security.declarePublic('getSiteLayoutTree')
    def getSiteLayoutTree(self, simple=None):
        """
	Returns site layout as an array. A row does have three entries: level, key, german name
	"""
        siteLayout = self.getSiteLayout();
        topicmap = self.getCMFIFileTopics()
        rows= []
        for key in siteLayout.keys():
            row = []
            row.append(0);
            row.append(key[key.find('/')+1:len(key)])  # english name as key/id
            row.append(key[0:key.find('/')])           # german name
            if topicmap and row[1] in topicmap.keys():
                row.append(topicmap[row[1]])
            else:
                row.append('')
            rows.append(row)
            if siteLayout[key] and (not simple or key=='Dienste/centraloffice'):
                for sublevel in siteLayout[key].split(','):     
                    row = []
                    row.append(1)               
                    row.append(sublevel[sublevel.find('/')+1:len(sublevel)])
                    row.append(sublevel[0:sublevel.find('/')])
                    if topicmap and row[1] in topicmap.keys():
                        row.append(topicmap[row[1]])
                    else:
                        row.append('')
                    rows.append(row)
        return rows                    
    
    security.declarePublic('getSiteLayout')
    def getSiteLayout(self):
        """
	Returns site layout as dictionary defined in global cmfi properties under key 'cmfi_site_layout'
	"""
	settings = self.portal_properties.cmfi_properties.cmfi_site_layout 
        siteLayout = {}
        for line in settings: 
            index = line.find(':')
            length = len(line)
            # check position in ':' -> if it is not set or it is at the end of the line means that 
            # no subprocesses are given
            if index>0 and index<length:
                siteLayout[line[0:index]] = line[index+1:length]
            else:
                siteLayout[line] = None
        return siteLayout

#####################################################################################################
# GET functions 
# -> functions return collected information about cmfi content
#####################################################################################################
    
    security.declarePublic('getCMFIFileTopics')
    def getCMFIFileTopics(self):
	"""
	Returns CMFI file topics
	"""
        catalog = getToolByName(self, 'portal_catalog')
        # search for the topics
        entries = catalog(meta_type = 'CMF CMFIFileTopic', review_state = 'public')   
        map = {}
        values = []
        if len(entries)>0:
            for entry in entries:
                topic = entry.getObject()
                for subject in topic.subject:
                  if subject != '__process':
                    if subject in map.keys():
                      values = map[subject]
                      values.append(topic.title+"|"+topic.id)
                    else:
                      values = []
                      values.append(topic.title+"|"+topic.id)
                    map[subject] = values
            return map
        else:
            return None      

    security.declarePublic('getDivisionsFromProcess')
    def getDivisionsFromProcess(self, process):
        """
        Returns all divisions of the given process
        """
        line = self.getSiteLayout()[process]
        divisions = []
        for entry in line.split(','):
            divisions.append(entry[:entry.find('/')])
        return divisions
    
    security.declarePublic('getCentralOffice')
    def getCentralOffice(self):
        """
	Returns all divisions of the centraloffice extracted from the siteLayout
	"""
        return self.getDivisionsFromProcess('Dienste/centraloffice')    
       
    security.declarePublic('getEmailFromMemberdata')
    def getEmailFromMemberdata(self, userid):
        """
	Returns the email set in the memberdata area
	"""
        membership = getToolByName(self, 'portal_membership')
        member = membership.getMemberById(userid)
        if member != None:
            return member.email
        else:
            return None                       
   
    security.declarePublic('getProcessFromURL')
    def getProcessFromURL(self, url):
        """
        extracts the current process from 'url'.
	e.g: http://localhost:8080/Intranet/centraloffice/hr_management
        * Process: centraloffice.hr_management
        """                
        # construct query, complex version
        url_split = url.split('/')
        count = len(url_split) - 2 
        search = url_split[count-1] + '.' + url_split[count]               
        active_processes = self.getSiteLayoutTreeAsInPortalActions(simple='yes')
	for process in active_processes:
	    if process[1] == search:
	        return search
        # nothing found...try simple version
        active_processes = self.getSiteLayout()
        for process in active_processes:               
            process = process[process.find('/')+1:]
            if url.find(process) > 0:
                return process	    
        return None        

#####################################################################################################
# SET function
# -> function to change portal information
#####################################################################################################
    
    security.declarePublic('setEmailForMember')
    def setEmailForMember(self, userid, email):
        """
	Set the email in the memberdata area if this is not set already
	"""	
        membership = getToolByName(self, 'portal_membership')
        member = membership.getMemberById(userid)
        if member:
            member.setProperties(email = email)

#####################################################################################################
# CHECK function
# -> used to check user permissions on special objects
#####################################################################################################
	
    security.declarePublic('isUserCMFIProfileReviewer')
    def isUserCMFIProfileReviewer(self, user):
        """
	Returns true if user is able to review/edit CMFIProfile elements
	"""
        reviewerlist = getattr(self.portal_properties.cmfi_properties,'cmfi_cmfiprofile_reviewer')
        for reviewer in reviewerlist:
            if 'group_' + reviewer in user.getGroups():
                return 1
        return 0

    security.declarePublic('isUserCMFIHomeReviewer')
    def isUserCMFIHomeReviewer(self, user):
        """
	Returns true if user is able to review/edit home objects (Event, News, CMFIAdmPhone)
	"""
        reviewerlist = getattr(self.portal_properties.cmfi_properties,'cmfi_cmfihome_reviewer')
        for reviewer in reviewerlist:
            if 'group_' + reviewer in user.getGroups():
                return 1
        return 0
   
#####################################################################################################
# Helper Functions
#####################################################################################################    
    
    security.declarePublic('castToArray')
    def castToArray(self, thisType):
	"""
	Typecast to an array
	"""
        if type(thisType) != type([]):
            return [thisType]
        else:
            return thisType 

    security.declarePublic('redirect')
    def redirect(self, obj, form, msg=None):
        """
        Redirect helper, used in workflow scripts
        """
        r = obj.REQUEST.RESPONSE
        url = '%s/%s' % (obj.absolute_url(), form)
        if msg:
            url += '?portal_status_message=%s' % quote_plus(msg)
        r.redirect(url)
        
InitializeClass(CMFITool)

#####################################################################################################

