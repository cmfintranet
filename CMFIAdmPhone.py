# -*- coding: UTF-8 -*-
# -*- Mode: Python; py-indent-offset: 4 -*-
# Authors:  Reto Buerki <reto.buerki (at) fhso.ch> 
#
# $Id$
# $HeadURL$

import os, urllib, string
from Globals import InitializeClass
from AccessControl import ClassSecurityInfo, getSecurityManager
from Products.CMFDefault.DublinCore import DefaultDublinCoreImpl
from Products.CMFDefault.Document import Document
from Products.CMFCore.WorkflowCore import WorkflowAction

# Import permission names
from Products.CMFCore import CMFCorePermissions

factory_type_information = (
    {'id': 'CMFIAdmPhone',
     'meta_type': 'CMF CMFIAdmPhone',
     'icon': 'user.gif',
     'description': ('CMFI Administrative Phones to include into a CMF Site.'),
     'product': 'CMFIntranet',
     'factory': 'addCMFIAdmPhone',
     'immediate_view': 'cmfi_admphone_edit_form',
     'actions': ({'id': 'view',
                  'name': 'View',
                  'action': 'cmfi_admphone_view',
                  'permissions':  (CMFCorePermissions.View,)},
                 {'id': 'edit',
                  'name': 'Edit',
                  'action': 'portal_form/cmfi_admphone_edit_form',
                  'permissions': (CMFCorePermissions.ModifyPortalContent,)},                 
                 ),
     },
    )

def addCMFIAdmPhone(self
                    , id
                    , title = ''
                    , telDirect = ''
                    , place = ''
                    , description = ''
                    , text = ''
                    , text_format = ''
                    , REQUEST=None):
    """Create an empty CMFIAdmPhone"""
    cmfi_admphone_object = CMFIAdmPhone(id,title,telDirect,description,text,text_format)
    self._setObject(id, cmfi_admphone_object)

class CMFIAdmPhone(Document):
    """A Administrative Phone Item"""

    meta_type='CMF CMFIAdmPhone'
    
    __implements__ = Document.__implements__ #redundant, but explicit
    
    security = ClassSecurityInfo()
    security.declareObjectPublic()
    def __init__(self
                 , id
                 , title=''             # title of the phone entry within the system
                 , telDirect=''
                 , place=''
                 , description=''
                 , text=''
                 , text_format='html'):
        """Initialize an instance of the class"""
        DefaultDublinCoreImpl.__init__(self)
        self.id = id
        self.title = title
        self.telDirect = telDirect
        self.place = place
        self.description = description
        self.text = text
        self.tex_format = text_format
        
    security.declareProtected(CMFCorePermissions.ModifyPortalContent, 'edit')
    def edit(self
             , title
             , telDirect
             , place
             , text=''
             , description=None
             , text_format=None):
        """Change the instance's properties """
        self.title = title
        self.telDirect = telDirect
        self.place = place
        if text_format is None:
            text_format = getattr(self, 'text_format', 'html')
        if description is not None:
            self.setDescription(description)
        Document.edit(self,text_format,text)
        
        self.reindexObject()
 
    security.declarePublic('formatTelNumber')
    def formatTelNumber(self, telNr):
        '''format given telephone number to a format like 062 3882 541'''
        telNr = string.strip(string.replace(telNr,' ',''))
        tel = telNr[:3] + ' ' + telNr[3:7] + ' ' + telNr[7:] 
        return tel
    
    edit = WorkflowAction(edit) 

    ### Content accessor methods
    security.declareProtected(CMFCorePermissions.View, 'SearchableText')
    def SearchableText(self):
        """ Used by the catalog for basic full text indexing """
        return "%s %s %s %s" % ( self.title
                            , self.description
                            , self.text
                            , self.place
                            )
    
InitializeClass(CMFIAdmPhone)
    
