#!/bin/bash
#
# this shell script is used to pack a release tarball
# of the product
#
# $Id$
# $HeadURL$

# globals
PRODUCT=CMFIntranet
VERSION=`cat version.txt`
DISTRO=$PRODUCT-$VERSION.tar.gz

echo -n "Packing $DISTRO..."

# remove all SVN directories from the distribution
find . -name .svn -depth -exec rm -fr {} \;

# remove all temporary and compiled files
find . -name "#*#" -exec rm -f {} \;
find . -name "*~" -exec rm -f {} \;
find . -name "*.pyc" -exec rm -f {} \;

# cleanup older version
if [ -e ../$DISTRO ]
then
    rm ../$DISTRO
fi

# make the distro
(cd ..; tar czf $DISTRO $PRODUCT)
echo "DONE"
