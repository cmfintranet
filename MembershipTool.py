# -*- coding: UTF-8 -*-
# -*- Mode: Python; py-indent-offset: 4 -*-
# Authors:  Reto Buerki <reto.buerki (at) fhso.ch> 
#
# $Id$
# $HeadURL$

from Products.CMFCore.utils import getToolByName
from Products.CMFPlone.MembershipTool import MembershipTool as BaseTool
from Products.CMFDefault.MembershipTool import DEFAULT_MEMBER_CONTENT
from Products.CMFPlone.PloneBaseTool import PloneBaseTool
from AccessControl import ClassSecurityInfo
from OFS.Image import Image
from Globals import InitializeClass
from Products.CMFCore.CMFCorePermissions import View
from CMFIProfile import addCMFIProfile
from Products.CMFPlone.PloneUtilities import translate

class MembershipTool( PloneBaseTool,BaseTool ):
    """ Plone customized Membership Tool """
    meta_type='CMFI Membership Tool'
    cmfi_tool = 1
    security = ClassSecurityInfo()

    __implements__ = (PloneBaseTool.__implements__, BaseTool.__implements__, )    

    def createMemberarea(self, member_id):
        """
        since we arent using PortalFolders and invokeFactory will not work
        we must do all of this ourself. ;(
        """
        parent = self.aq_inner.aq_parent
        members =  getattr(parent, 'Members', None)
        if members is not None and not hasattr(members, member_id):
            f_title = "%s's Home" % member_id
            members.manage_addPloneFolder( id=member_id, title=f_title )
            f=getattr(members, member_id)
            # Grant ownership to Member
            acl_users = self.__getPUS()
            user = acl_users.getUser(member_id)
            if user is not None:
                user= user.__of__(acl_users)
            else:
                from AccessControl import getSecurityManager
                user= getSecurityManager().getUser()
                # check that we do not do something wrong
                if user.getUserName() != member_id:
                    raise NotImplementedError, \
                        'cannot get user for member area creation'
            f.changeOwnership(user)
            f.manage_setLocalRoles(member_id, ['Owner'])
            # Create Member's home page.
            # DEFAULT_MEMBER_CONTENT ought to be configurable per
            # instance of MembershipTool.
            addCMFIProfile( f
                       , id = 'index_html'
                       , userid = member_id+"'s Profile"
                       , text_format = "structured-text"
                       )
            f.index_html._setPortalTypeName( 'CMFIProfile' )
            # profiles can only be deleted by managers
            f.index_html.manage_permission('Delete objects', ['Manager'], 0)
	    # grant 'delete objects' right in home folder to reviewers
            f.manage_permission('Delete objects', ['Reviewer'], 1)
            # Overcome an apparent catalog bug.
            f.index_html.reindexObject()
            wftool = getToolByName( f, 'portal_workflow' )
            wftool.notifyCreated( f.index_html )
            #XXX the above is copy/pasted from CMFDefault.MembershipTool only because
            #its not using invokeFactory('Folder') -- FIX IT!
            
            #XXX Below is what really is Plone customizations
            member_folder=self.getHomeFolder(member_id)
	    member_folder.title = translate(
                'plone', 'title_member_folder',
                {'member': member_id}, self,
                default = "%s's Home" % member_id)
            member_folder.description = translate(
                'plone', 'description_member_folder',
                {'member': member_id}, self,
                default = 'Home page area that contains the items created ' \
                'and collected by %s' % member_id)
        
            member_folder.manage_addPloneFolder('.personal', 'Personal Items')
            personal=getattr(member_folder, '.personal')
            personal.description = "contains personal workarea items for %s" % member_id
            personal.changeOwnership(user)
            personal.manage_setLocalRoles(member_id, ['Owner'])
            
            catalog = getToolByName(self, 'portal_catalog')
            catalog.unindexObject(personal) #dont add .personal folders to catalog

MembershipTool.__doc__ = BaseTool.__doc__

InitializeClass(MembershipTool)
