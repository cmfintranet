# -*- coding: UTF-8 -*-
# -*- Mode: Python; py-indent-offset: 4 -*-
# Authors:  Reto Buerki <reto.buerki (at) fhso.ch> 
#
# $Id$
# $HeadURL$

from Products.CMFCore.TypesTool import ContentFactoryMetadata
from Products.CMFCore.DirectoryView import addDirectoryViews
from Products.CMFCore.CMFCorePermissions import ManagePortal
from Products.CMFCore.CMFCorePermissions import ReviewPortalContent
from Products.CMFCore.utils import getToolByName
from Products.CMFCore.Expression import Expression
from Products.CMFCore.SkinsContainer import SkinPathError
from Products.CMFPlone import ToolNames
from Products.CMFIntranet import CMFIProfile, CMFIFileTopic, cmfi_profile_globals
from Products.CMFIntranet import CMFITool, MembershipTool, CMFIAdmPhone
from Products.CMFIntranet.Workflow.Workflow import install_workflow
from Products.CMFIntranet import config
from Products.CMFIntranet.config import *
from cStringIO import StringIO
import string

#####################################################################################################
# GLOBAL vars
#####################################################################################################

out = StringIO()

type_classes = (CMFIProfile,CMFIFileTopic,CMFIAdmPhone,)

cmfiskin = 'CMFI'

skins = ('cmfi_content',
         'cmfi_forms',
         'cmfi_images',
         'cmfi_scripts',
         'cmfi_scripts',
         'cmfi_form_scripts',
         'cmfi_templates',
         'cmfi_portlets',
         'cmfi_styles')

field_indexes =     ('name','address','firstname','title','token','userid')

keyword_indexes =   ('public_services',
		     'categories',
                     'divisions',
		     'services_centraloffice',
		    )

tools =             (
                     ('portal_cmfi_tool','CMFI Tool'),
		     ('portal_cmfi_config_tool', 'CMFI Config Tool'),
                     ('portal_membership','CMFI Membership Tool'), 
                     ('portal_calendar','CMFI Calendar Tool')
	            )

default_frontpage=r"""
<fieldset> 
This is CMFIntranet, Copyright (c) 2004-2005 <a target="_blank" href="http://www.axionic.ch">axionic</a><br><br>
<p align="center"><img src="axionic_logo.jpg"></p><br><br>
Please send any comments, corrections or bugs to <a href="mailto:info@axionic.ch">info@axionic.ch</a><br><br>
</fieldset>
"""

#####################################################################################################
# HELPER functions
#####################################################################################################

def customize_membership_tool(self):
    """
    Customize actions for members 
    """
    mt=getToolByName(self, 'portal_membership')
    mt.addAction('news'
		    ,'News'
		    ,'string:${portal_url}/news'
		    ,'member'
		    ,'View'
		    ,'user'
		    , visible=1)   
    mt.addAction('help'
                    ,'help'
                    ,'string:${portal_url}/help'
                    ,'member'
                    ,'View'
                    ,'user'
                    , visible=1)
    mt.addAction('search'
                    ,'search'
                    ,'string:${portal_url}/search_form'
                    ,'member'
                    ,'View'
                    ,'user'
                    , visible=1)
    mt.addAction('intranet_setup'
		    ,'Intranet Setup'
		    ,'string: ${portal_url}/plone_control_panel'
		    ,'', # condition
                    ManagePortal
		    ,'user'
		    , visible=1)
    new_actions=[]
    for a in mt._cloneActions():
        if a.id=='preferences':
            a.action=Expression('string:${portal_url}/plone_memberprefs_panel')
            new_actions.insert(0, a)
        if a.id=='addFavorite': 
            a.visible=0
            new_actions.insert(1,a)
        if a.id=='mystuff': 
            a.title='My Folder'
            new_actions.insert(0, a)
        elif a.id=='myworkspace':
            new_actions.insert(1, a)
        elif a.id=='logout':
            new_actions.append(a)
        else:
            new_actions.insert(len(new_actions)-1,a)
            
    mt._actions=new_actions   
    # hide 'confidential' folders in nav
    prop_tool=getToolByName(self, 'portal_properties')
    p=prop_tool.navtree_properties
    #p.manage_changeProperties(metaTypesNotToList=['PloneCollectorNG','TempFolder','ForumFolder'])
    p.manage_changeProperties(idsNotToList=['confidential'], topLevel=1)
    out.write('[*] Membership Tool customized\n')

def register_configlets(self):
    """
    Register control panel configlets
    """
    # register configlets  
    try:
        self.portal_controlpanel.registerConfiglet(**cmfi_configlet_props)
        self.portal_controlpanel.registerConfiglet(**cmfi_configlet_mngt)
    except:
        pass
    
def customize_action_tool(self):
    """
    Add cmfi specific actions
    """
    mt=getToolByName(self, 'portal_actions')  
    ptool = getToolByName(self, 'portal_cmfi_tool')
    layoutDic = ptool.getSiteLayout()

    # enable/disable some actions
    new_actions=[]
    for a in mt._cloneActions():
	if getattr(a,'id','') in ('index_html', 'Members', 'search_form'): 
            a.visible=0
            new_actions.insert(1,a)
        elif a.id=='news': 
            a.title='Home'
            new_actions.insert(0, a)
        else:
            new_actions.insert(len(new_actions)-1,a)
    mt._actions=new_actions 
    # add content status object tab
    mt.addAction('content_status_history'
		    ,'State'
		    ,'string:${object_url}/content_status_history'
		    ,'python:object and portal.portal_workflow.getTransitionsFor(object, object.getParentNode())'
		    ,'View'
		    ,'object_tabs')
    
    # Add our template portal_tab 
    data = {}
    titles={}
    # TODO: write a function for this
    for key, value in layoutDic.items():
        tit, k = key.split('/')
        titles[k] = tit
        data[k] = []
        if value:
            for part in value.split(','):
                l,r = part.split('/')
                data[k].append(r)
                titles[r] = l
    # build action id list
    actions = mt.listActions()
    action_ids = []
    for a in actions:
        action_ids.append(getattr(a, 'id'))
    foldernames = data.keys()
    for folder in foldernames: 
        # test if this action is already there
	if not folder in action_ids:
            mt.addAction(folder
                        ,titles[folder]
                        ,'string: $portal_url/' + folder
                        ,''
                        ,'View'
                        ,'portal_tabs'
                        , visible=1)   
    
    out.write('[*] Action Tool customized\n')

def customize_types_tool(self):
    """
    Customize types tool
    """
    portal = getToolByName(self, 'portal_url').getPortalObject()
    types = getToolByName(portal, 'portal_types')    	
    for t in types.objectValues():
        _actions = t._cloneActions()
        # disable shared tab
	for a in _actions:
	    if a.id == 'local_roles':
	        a.visible=0
	t._actions = _actions
    	
    out.write('[*] Types Tool customized\n')
    

def customize_index_htmls(self, portalroot):
    """
    Customize all index_html pages
    """
    index_html_proc= portalroot.portal_skins.cmfi_templates.index_html_proc
       
    # customize index_html forms    
    if not hasattr(portalroot.portal_skins.custom, 'index_html_proc'):
        index_html_proc.manage_doCustomize( folder_path='custom' ) 
    
def customize_forms(self, portalroot):
    """
    Customize forms
    """    
    # customize personalize_form
    personalize = portalroot.portal_skins.plone_form_scripts.personalize
    try:
        personalize.manage_doCustomize( folder_path='custom' )
    except:
        pass
    custompersonalize = portalroot.portal_skins.custom.personalize
    custompersonalize.manage_proxy(['Manager'])

    # customize css and base_props
    #dir = portalroot.portal_skins.cmfi_styles
    #css = getattr(dir, 'ploneCustom.css')
    #try:
    #    css.manage_doCustomize( folder_path='custom')
    #except:
    #    pass
    #props = getattr(dir, 'base_properties')
    #try:
    #    props.manage_doCustomize( folder_path='custom')  
    #except:
    #    pass
	
    out.write('[*] Forms customized\n')

def setup_default_slots(self, portal):
    """ 
    Sets up the slots on objectmanagers
    """
    #add the slots to the portal folder
    if hasattr(portal,'right_slots'):    
        portal._delProperty('right_slots')
        right_slots=('here/portlet_review/macros/portlet'
                , 'here/portlet_recent/macros/portlet')
        portal._setProperty('right_slots', right_slots, 'lines')   
    if hasattr(portal,'left_slots'):    
        portal._delProperty('left_slots')
        left_slots=('here/portlet_calendar/macros/portlet'
                , 'here/portlet_telephone/macros/portlet'
                , 'here/portlet_gallery/macros/portlet')
        portal._setProperty('left_slots', left_slots, 'lines')     
    
def register_validator(self, validator):
    """
    Register given validator
    """
    formtool = getToolByName(self, 'portal_form')
    formtool.setValidators(validator[0], [validator[1], validator[2]])
    out.write('[*] Validator %s is registered with the form tool\n' % (validator[0]))
    
def register_action(self, action):
    """
    Register action with navigation tool
    """
    navtool = getToolByName(self, 'portal_navigation')
    navtool.addTransitionFor(action[0],action[1],action[2],action[3])
    out.write('[*] Action %s.%s.%s registered with the navigation tool\n' % (action[0],action[1],action[2]))

def remove_action(self, action):
    """
    Remove action from navigation tool
    """
    navtool = getToolByName(self, 'portal_navigation')
    navtool.removeTransitionFrom(action[0],action[1],action[2])
    out.write('[*] Action %s.%s.%s removed from navigation tool\n' % (action[0],action[1],action[2]))
    
def register_type(self, type):
    """
    Register type defined in fti in the types tool
    """
    typestool = getToolByName(self, 'portal_types')
    if type['id'] not in typestool.objectIds():
        cfm = apply(ContentFactoryMetadata, (), type)
        typestool._setObject(type['id'], cfm)
        out.write('[*] Registered %s with the types tool\n' % type['id'])
    else:
        out.write('[!] Object "%s" already existed in the types tool\n' % (type['id']))

def unregister_type(self, type):
    """
    Un-Register type in the types tool
    """
    typestool = getToolByName(self, 'portal_types')
    if type in typestool.objectIds():
        typestool._delObject(type)
        out.write('[*] Un-Registered %s with the types tool\n' % type)
    else:
        out.write('[!] Object "%s" did not exist in the types tool\n' % type)
        
def add_skin(self, skinstool, skinname):
    """
    Add a skin to portal_skins, should be done right before where 'content' is placed.  
    Otherwise, we append it to the end.
    """
    skins = skinstool.getSkinSelections()
    for skin in skins:
        path = skinstool.getSkinPath(skin)
        path = map(string.strip, string.split(path,','))
        if skinname not in path:
            try: 
                path.insert(path.index('plone_ecmascript'), skinname)
            except ValueError:
                path.append(skinname)

            path = string.join(path, ', ')
            # addSkinSelection will replace existing skins as well.
            skinstool.addSkinSelection(skin, path)
            out.write("[*] Added %s to %s skin\n" % (skinname, skin))
        else:
            out.write("[!] Skipping %s skin, %s is already set up\n" % (skin, skinname))
         
def remove_skin(self, skinstool, skinname):
    """
    Remove a skin from portal_skins
    """
    skinstool = getToolByName(self, 'portal_skins')
    skins = skinstool.getSkinSelections()
    for skin in skins:
        path = skinstool.getSkinPath(skin)
        path = map(string.strip, string.split(path,','))
        if skinname in path:
            path.remove(skinname)
            path = string.join(path, ', ')
            # addSkinSelection will replace existing skins as well.
            skinstool.addSkinSelection(skin, path)
            out.write("[*] Removed %s from %s skin\n" % (skinname, skin))

    if skinname in skinstool.objectIds():
        skinstool._delObject(skinname)
        out.write('[*] Removed %s from the skinstool\n' % skinname)

def add_index(self, portalcatalog, indexname, indextype):
    """
    Add a index to portal_catalog
    """
    if not indexname in portalcatalog.Indexes.objectIds():
        portalcatalog.addIndex(indexname, indextype)
        out.write('[*] Added %s as %s to portal catalog\n' % (indexname, indextype))
    else:
        out.write('[!] %s already exists as %s\n' % (indexname, indextype))
        
def remove_index(self, portalcatalog, indexname, indextype):
    """
    Remove index from portal_catalog
    """  
    if indexname in portalcatalog.Indexes.objectIds():
        portalcatalog.delIndex(indexname)
        out.write('[*] Removed index %s from portal catalog\n' % indexname)
    else:
        out.write('[!] Index %s does not exist in portal catalog\n' % indexname)
        
def add_tool(self, portalroot, tool):
    """
    Add a given tool to a portal
    """
    if tool[0] in portalroot.objectIds():
        portalroot.manage_delObjects( tool[0] )

    addCMFITool = portalroot.manage_addProduct[PROJECTNAME]
    addCMFITool.manage_addTool(tool[1], None)
    out.write('[*] Created Tool %s\n' % (tool[1]))
    
        
def remove_tool(self, portalroot, tool):
    """
    Remove a given tool from a portal
    """
    if tool[0] in portalroot.objectIds():
        portalroot._delObject(tool[0])
        out.write('[*] Removed Tool %s\n' % tool[0])
    else:
        out.write('[!] Tool %s does not exist\n' % (tool[0]))

def setup_cmfi_skin(self, sk_tool):
    """
    Sets up the cmfi specific skins
    """
    path=[elem.strip() for elem in sk_tool.getSkinPath('Plone Default').split(',')]
    for cmfidir in skins:
        try:
            path.insert( path.index( 'custom')+1, cmfidir )
        except ValueError:
            path.append( cmfidir )
    path=','.join(path)
    try:
        sk_tool.addSkinSelection(cmfiskin, path, test=1)
        out.write('[!] Skin CMFI is already setup\n')
    except SkinPathError:
        sk_tool.addSkinSelection(cmfiskin, path, make_default=1)
        addDirectoryViews( sk_tool, 'skins', cmfi_profile_globals )
        out.write('[*] Skin CMFI added and setup as default skin\n')
	
def set_cmfi_properties(self):
    """
    Register CMFIntranet Properties
    """
    if not hasattr(self.portal_properties, 'cmfi_properties'):
        self.portal_properties.addPropertySheet(
            'cmfi_properties', 'CMFIntranet properties')
    props = self.portal_properties.cmfi_properties
    for prop, tp, val in config.cmfi_properties:
        if not props.hasProperty(prop):
            props._setProperty(prop, val, tp)
    out.write('[*] Successfully installed global properties\n')
    
def set_cmfi_workflow(self, wf_tool):
    """
    Set CMFI Workflow as Default and set profile workflow
    """
    wf_tool.setDefaultChain('cmfi_workflow')
    wf_tool.setChainForPortalTypes( ('CMFIProfile', 'Event', 'CMFIAdmPhone'), 'cmfi_simple_workflow')
    wf_tool.updateRoleMappings()

def unset_cmfi_workflow(self, wf_tool):
    """
    Set the default Workflow to plone_workflow
    """
    wf_tool.setDefaultChain('plone_workflow')    
    wf_tool.updateRoleMappings()

def manage_folder_and_groups(self):
    """
    Adds all Folder and Groups
    """
    groupsuffixes = [                   
                    ('_member', ['Member'], '', 0, '', '') ,
                    ('_reviewer',['Reviewer'], ['Reviewer'], 0, ['Add portal content'], 'cms-home'),
                    ('_manager', ['Manager'], ['Manager'], 1, '', '')
                    ]

    portal = getToolByName(self, 'portal_url').getPortalObject()
    ptool = getToolByName(self, 'portal_cmfi_tool')
    layoutDic = ptool.getSiteLayout()
    userfolder = getattr(portal, "acl_users")       
    
    cfolder = getattr(portal, 'cms-home', None)
    if not cfolder:
        portal.invokeFactory('Folder', 'cms-home')
        out.write('[*] Created folder cms-home\n')
    getattr(portal, 'cms-home').manage_changeProperties(REQUEST={'title':'CMFI-News'})   
    
    # we need a group for news that cary only the cms-home tag.
    try:
        thisgroupname = 'cms-home' + '_reviewer'
        userfolder.userFolderAddGroup(thisgroupname, ['Reviewer'])
        out.write('[*] Added group %s\n' % thisgroupname)
    except:
        pass

    folder = getattr(portal, 'cms-home')
    roles = ['Reviewer']    	
    # does the role need a special permission in the target folder
    # yes, if role wants to copy
    targetpermission = ['Add portal content']
    if len(targetpermission):
        for role in roles:
            pofrole = []
            for r in folder.permissionsOfRole(role):
                if r['selected'] == 'SELECTED':
                    pofrole.append(r['name'])
            folder.manage_role(role, pofrole + targetpermission)
    data = {}
    titles={}

    for key, value in layoutDic.items():
        tit, k = key.split('/')
        titles[k] = tit
        data[k] = []
        if value:
            for part in value.split(','):
                l,r = part.split('/')
                data[k].append(r)
                titles[r] = l
                #map(lambda x: x.split('/')[1], value.split(','))
		
    foldernames = data.keys()
    for foldername in foldernames:
        folder = getattr(portal, foldername, None)
        if not folder:
            portal.invokeFactory('Folder', foldername)
            folder = getattr(portal, foldername)
        folder.manage_changeProperties(REQUEST={'title':titles[foldername]})
        folder.manage_permission('View', ['Authenticated'], 1)
        folder.manage_permission('Access contents information', ['Authenticated'], 1)
        # make sure the state is published (avoids problems with reviewers)
        try:
            self.portal_workflow.doActionFor(folder,'publish')
        except:
            pass
		
        if folder.hasProperty('left_slots'):   
            folder.manage_delProperties(['left_slots',])
        left_slots=('here/portlet_navigation/macros/portlet'
                    , 'here/portlet_news/macros/portlet'
		    , 'here/portlet_slinks/macros/portlet'
                    , 'here/portlet_processes/macros/portlet'
                    , 'here/portlet_files/macros/portlet')
        folder.manage_addProperty('left_slots', left_slots, 'lines')           
        
        try:
            copy_info  = portal.portal_skins.custom.manage_copyObjects(('index_html_proc'))
            folder.manage_pasteObjects(copy_info)
            folder.manage_renameObjects(['index_html_proc'],['index_html'])
            index_html = getattr(folder, "index_html")
            index_html.manage_permission('View', ['Authenticated'], 0)
            index_html.manage_permission('Access contents information', ['Authenticated'], 0)
        except:
            pass
	
	cfolder = getattr(folder, 'confidential', None)
        if not cfolder:
            folder.invokeFactory('Folder', 'confidential')
            cfolder = getattr(folder, 'confidential')
            cfolder.manage_permission('View', ['Authenticated','Manager','Owner','Reviewer'], 0)
            cfolder.manage_permission('Access contents information', ['Authenticated','Manager','Owner','Reviewer'], 0)
            cfolder.manage_permission('List folder contents', ['Manager','Reviewer'], 0)
        cfolder.manage_changeProperties(REQUEST={'title':'Vertraulich'})
        # add groups and roles        
	out.write('>> Creating groups and roles...\n')
        for suffix, roles, toportal, toconfidential, targetpermission, commonfolders in groupsuffixes:
            thisgroupname = foldername + suffix
            try:
                userfolder.userFolderAddGroup(foldername + suffix, roles)
            except:
                pass            
            # add groups and roles for confidential
            if toconfidential:                
	        # does the role need a special permission in the confidential folder
                # yes, if role wants to copy
                if len(targetpermission):
                    for role in roles:
                        pofrole = []
                        for r in cfolder.permissionsOfRole(role):
                            if r['selected'] == 'SELECTED':
                                pofrole.append(r['name'])
                        cfolder.manage_role(role,pofrole + targetpermission)
	    else:
                # does the role need a special permission in the target folder
                # yes, if role wants to copy
                if len(targetpermission):
                    for role in roles:
                        pofrole = []
                        for r in folder.permissionsOfRole(role):
                            if r['selected'] == 'SELECTED':
                                pofrole.append(r['name'])
                        folder.manage_role(role, pofrole + targetpermission)
            
            # do we have to deal with common folders where every reviewer can publish
            if len(commonfolders):
                common = portal
                for part in commonfolders.split('/'):
                    try:
                        common = getattr(common,part)
                    except:
                        raise                
	
	    out.write('[*] Created %s\n' %thisgroupname)
	    
        # *******************************************************************************
        # divisions folder
        # *******************************************************************************
        #deal with subfolders
        subfolders = data[foldername]
        for subfoldername in subfolders :
            origname = subfoldername
            prefix = ''
            if subfoldername.find('.') > -1:
                prefix = subfoldername.split(".")[0] + "."
            subfoldername = subfoldername.split(".")[-1]
            subfolder =  getattr(folder, subfoldername, None)
            if not subfolder:
                folder.invokeFactory('Folder', subfoldername)
                subfolder = getattr(folder, subfoldername)              
            germanTitle = titles[origname]
            subfolder.manage_changeProperties(REQUEST={'title':titles[origname]})
            subfolder.manage_permission('View', ['Authenticated'], 1)
            subfolder.manage_permission('Access contents information', ['Authenticated'], 1)
            
            # make sure the state is published (avoids problems with reviewers)
            try:
                self.portal_workflow.doActionFor(subfolder,'publish')
            except:
                pass

            # install index page
            try:
                copy_info  = portal.portal_skins.custom.manage_copyObjects(('index_html_proc'))
                subfolder.manage_pasteObjects(copy_info)
                subfolder.manage_renameObjects(['index_html_proc'],['index_html'])
            except:
                pass
	    	    
            # tag as special folder
            subjects = ['__subprocess']
	    self.plone_utils.editMetadata(subfolder, subject=subjects)               

            try:
                cfolder = subfolder['confidential']
            except:
                subfolder.invokeFactory('Folder', 'confidential')
                cfolder = getattr(subfolder, 'confidential')
                cfolder.manage_permission('View', ['Authenticated','Manager','Owner','Reviewer'], 0)
                cfolder.manage_permission('Access contents information', ['Authenticated','Manager','Owner','Reviewer'], 0)
                cfolder.manage_permission('List folder contents', ['Manager','Reviewer'], 0)
            cfolder.manage_changeProperties(REQUEST={'title':'Vertraulich'})
            for suffix, roles, toportal, toconfidential, targetpermission, commonfolders in groupsuffixes:
                thisgroupname = prefix + subfoldername + suffix
                try:
                    userfolder.userFolderAddGroup(thisgroupname, roles)
                except:
                    pass
                # does the role need a special permission in the target folder
                # yes, if role wants to copy
                if len(targetpermission):
                    if toconfidential:
                        # we have to add rights to the confidential folder
                        subfolder = cfolder
                    for role in roles:
                        pofrole = []
                        for r in subfolder.permissionsOfRole(role):
                            if r['selected'] == 'SELECTED':
                                pofrole.append(r['name'])
                        subfolder.manage_role(role, pofrole + targetpermission)                    

    
#####################################################################################################
# INSTALL/UNINSTALL functions
# -> called by portal_quickinstall
#####################################################################################################

def install(self):
    """
    Register all CMFIntranet components with the necessary tools
    """
    out.write('\n')
    out.write('===============================\n')
    out.write('CMFFIntranet Installation\n')
    out.write('===============================\n')
    skinstool = getToolByName(self, 'portal_skins')
    portalroot = getToolByName(self, 'portal_url').getPortalObject()
    portalcatalog = getToolByName(self, 'portal_catalog')

    # Set up the types
    for c in type_classes:
        t = c.factory_type_information
        if type(t) == type(()):
            for fti in t:
                register_type(self, fti)
        else:
            register_type(self, t)

    # add a new skin to the skin_tool and set this path as default
    setup_cmfi_skin(self, skinstool)
       
    # Now all indexes to the portal_catalog
    for index in field_indexes:
        add_index(self, portalcatalog, index, 'FieldIndex')

    for index in keyword_indexes:
        add_index(self, portalcatalog, index, 'KeywordIndex')        
       
    # Now install the tools
    for tool in tools:
        add_tool(self, portalroot, tool)        
        
    ## register CMFI props
    set_cmfi_properties(self)
        
    # Customize Tools and index_html
    customize_index_htmls(self, portalroot)
    customize_membership_tool(self)
    customize_action_tool(self)
    register_configlets(self)
    customize_types_tool(self)    
               
    # Set permission on MailHost
    mailhost = portalroot.MailHost
    mailhost.manage_permission('Use mailhost services', ['Authenticated','Manager','Member'], 0)
    
    # Set permission on Members
    mailhost = portalroot.Members
    mailhost.manage_permission('View', ['Authenticated','Manager','Member','Reviewer'], 0)
    
    # set site properties
    props = portalroot.portal_properties.site_properties
    if hasattr(props,'localTimeFormat'):
        props._delProperty('localTimeFormat')
    if hasattr(props,'localLongTimeFormat'):
        props._delProperty('localLongTimeFormat')
    localTimeFormat=('%Y-%m-%d')
    localLongTimeFormat=('%Y-%m-%d %I:%M %p')
    props._setProperty('localTimeFormat', localTimeFormat, 'string')
    props._setProperty('localLongTimeFormat', localLongTimeFormat, 'string')
    props.manage_changeProperties(default_charset='iso-8859-1')
    out.write('[*] Site-Properties: portal_properties.site_properties adjusted\n')
    
    setup_default_slots(self, portalroot)   
    customize_forms(self, portalroot)	
        
    ## add workflows
    install_workflow(self, out)
    out.write('[*] Workflow installed\n')
    wf_tool= getToolByName(self, 'portal_workflow')
    set_cmfi_workflow(self, wf_tool)
    out.write('[*] Default Workflow set to: "cmfi_workflow"\n')
    manage_folder_and_groups(self)
    out.write('[*] Installed User Folder and Groups\n')

    # reindex catalog and workflow settings
    portalroot.portal_catalog.refreshCatalog()
    portalroot.portal_workflow.updateRoleMappings()
	
    # install main page
    try:
        o = portalroot.index_html
        o._setPortalTypeName( 'Document' )
        o.setTitle('CMFIntranet Portal')
        o.setDescription('CMFIntranet Info Page')
        o.edit('text/html', default_frontpage)
        self.portal_workflow.doActionFor(o, 'forcepublish')
    except:
        pass
    
    return out.getvalue()
          
def uninstall(self):
    """
    Remove skins, types and special objects
    """
    out.write('\n')
    out.write('===============================\n')
    out.write('CMFIntranet Un-Installation\n')
    out.write('===============================\n')
    portalroot = getToolByName(self, 'portal_url').getPortalObject()
    skinstool = getToolByName(self, 'portal_skins')
    portalcatalog = getToolByName(self, 'portal_catalog')

    # uninstall configs
    self.portal_controlpanel.unregisterApplication(PROJECTNAME)

    for c in type_classes:
        t = c.factory_type_information
        if type(t) == type(()):
            for fti in t:
                unregister_type(self, fti['id'])
        else:
            unregister_type(self, t['id'])

    # Now all indexes to the portal_catalog
    for index in field_indexes:
        remove_index(self, portalcatalog, index, 'FieldIndex')
    for index in keyword_indexes:
        remove_index(self, portalcatalog, index, 'KeywordIndex')

    # Now uninstall the tool
    for tool in tools:
        remove_tool(self, portalroot, tool)

    # Now reinstall plone_membership default
    if 'portal_membership' not in portalroot.objectIds():
        manage_addTool=portalroot.manage_addProduct['CMFPlone'].manage_addTool
        manage_addTool(ToolNames.MembershipTool)            
            
    # Reset permission on MailHost
    mh = portalroot.MailHost
    mh.manage_permission('Use mailhost services',[],acquire=1)
    out.write('[*] Removed role member from using mail host services\n');
    
    # Set default Workflow back to Plone
    wf_tool= getToolByName(self, 'portal_workflow')
    unset_cmfi_workflow(self, wf_tool)
    out.write('[*] Default Workflow set to: "plone_workflow"')

    return out.getvalue()

#####################################################################################################

