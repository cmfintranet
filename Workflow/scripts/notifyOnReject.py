## Script (Python) "notifyOnReject"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=state_change
##title=notify submitter on reject

o=getattr(state_change, 'object')

submitter = o.owner_info()['id']
reviewer = context.portal_membership.getAuthenticatedMember().getId()
title = o.title
    
# send a notification to submitter
try:
    o.mailInfoOnReject(reviewer=reviewer,submitter=submitter,title=title)
except:
    pass
