## Script (Python) "setTransitionRoles"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=state_change
##title=set correct local roles

obj = getattr(state_change, 'object')
process, ok = obj.getProcessInfo()

# set local group for reviewer when target transition is pending
if state_change.new_state.id == 'pending':
    g = context.portal_groups.getGroupById(process + '_reviewer')
    obj.manage_setLocalRoles(g.getId(), ['Reviewer'])       
