## Script (Python) "moveOnPublish"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=state_change
##title=move document to process folder
o=getattr(state_change, 'object')

# set some local variables
submitter = o.owner_info()['id']
reviewer = context.portal_membership.getAuthenticatedMember().getId()
title = o.title

obj_parent=o.aq_parent
portal_root=context.portal_url.getPortalObject()

process, ok = o.getProcessInfo()

# look for the targetfolder by splitting the process in its parts
# centraloffice.hr_management becoms
# centraloffice
# hr_management
targetfolder = portal_root
for part in process.split('.'):
  errorstr = 'subfolder %s/%s does not exist. Document not moved' % (targetfolder.getId(), part)
  try:
    targetfolder = targetfolder[part]
  except:
    raise Exception( errorstr )

# we need to know what is the new states id. If it ends with confidential
# we have to copy the document to its confidential subdirectory
isconfidential = ( state_change.new_state.getId().endswith('confidential') )
if isconfidential:
   try:
       targetfolder = targetfolder.confidential
   except:
      raise Exception('subfolder %s/confidential does not exist. Document not moved' % targetfolder )

# change ownership to the process
# context.plone_utils.changeOwnershipOf(o, process.split('.')[0], 1)

# set allowDiscussion per Default to false
context.plone_utils.editMetadata(o,allowDiscussion='off')

# are we allredy in the target folder ??
if targetfolder.absolute_url() == obj_parent.absolute_url():
   return

if targetfolder and targetfolder.getTypeInfo().getId().index('Folder') > -1:
    # move it baby!
    targetfolder.manage_pasteObjects( obj_parent.manage_cutObjects(o.getId()) )

    newobj = targetfolder[o.getId()]

    # raise a ObjectMoved exception. This will recatalog the
    # new object, and return its second argument to the script
    # that was calling the execution of the transaction
    newobj.manage_permission('Modify portal content', ['Manager'], 0)
    if state_change.new_state.getId() == 'public':
       newobj.manage_permission('Access contents information', ['Manager', 'Member','Authenticated'], 0)
    else:
       newobj.manage_permission('Access contents information', ['Manager', 'Member'], 0)
  
    
    # send a notification to submitter of the project (who is the receiver of the email)
    try:
       context.mailPublicationOnSuccess(receiver=submitter, reviewer=reviewer, title=title, targetfolder=targetfolder.absolute_url())
       raise state_change.ObjectMoved(newobj, newobj)
    except:
       raise state_change.ObjectMoved(newobj, newobj)
