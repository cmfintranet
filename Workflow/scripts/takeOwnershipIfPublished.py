## Script (Python) "takeOwnershipIfPublished"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=state_change
##title=change ownership to actual user when a published document is retracted

o=getattr(state_change, 'object')

# delete local roles assigned during submit transition
roles = o.get_local_roles()
o.manage_delLocalRoles(roles)

mtool = context.portal_membership
member = mtool.getAuthenticatedMember()

# change ownership to the division
# this creates a plone dependency
# if this should be avoided you have to make an
# external script out of the following method
oldstate = state_change.old_state.getId()
if oldstate.startswith('public'):
	#context.plone_utils.changeOwnershipOf(o, member.getId(), 1)
	member_folder=mtool.getHomeFolder(member.getMemberId())

	if member_folder and member_folder.getTypeInfo().getId().index('Folder') > -1:
		# get parent folder
		obj_parent=o.aq_parent

	# move it baby!
	member_folder.manage_pasteObjects( obj_parent.manage_cutObjects(o.getId()) )
	newobj = member_folder[o.getId()]	

	raise state_change.ObjectMoved(newobj, newobj)
