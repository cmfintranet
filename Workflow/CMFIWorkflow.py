# -*- coding: UTF-8 -*-
# -*- Mode: Python; py-indent-offset: 4 -*-
# Authors:  Reto Buerki <reto.buerki (at) fhso.ch> 
#
# $Id$
# $HeadURL$

from Products.CMFCore.WorkflowTool import addWorkflowFactory
from Products.CMFCore.CMFCorePermissions import ReviewPortalContent
from Products.DCWorkflow.DCWorkflow import DCWorkflowDefinition

p_review = ReviewPortalContent

def setupCMFI_workflow(wf):
    "..."
    wf.setProperties(title='Default Workflow [CMFIntranet]')

    for s in ['private', 'pending', 'public', 'publicconfidential', \
            'publicintern', 'visible']:
        wf.states.addState(s)
    for t in ['hide', 'publishconfidential', 'publishcmfi', 'forcepublish', \
	    'publishintern', 'reject', 'retract', 'submit', 'show']:
        wf.transitions.addTransition(t)
    for v in ['action', 'actor', 'comments', 'review_history', 'time']:
        wf.variables.addVariable(v)
    for l in ('reviewer_queue',):
        wf.worklists.addWorklist(l)
    for p in ('Access contents information', 'Modify portal content', 'View', 'View management screens'):
        wf.addManagedPermission(p)
        

    ## Initial State
    wf.states.setInitialState('private')

    ## States initialization
    sdef = wf.states['private']
    sdef.setProperties(title="""Visible and editable only by owner""",
                       transitions=('show', 'submit', 'publishcmfi', 'forcepublish', 'publishintern', 'publishconfidential'))
    sdef.setPermission('Access contents information', 0, ['Manager', 'Owner'])
    sdef.setPermission('Modify portal content', 0, ['Manager', 'Owner'])
    sdef.setPermission('View', 0, ['Manager', 'Owner'])
    sdef.setPermission('Change portal events', 0, ['Manager'])
    
    sdef = wf.states['pending']
    sdef.setProperties(title="""Waiting for reviewer""",
                       transitions=('retract', 'reject', 'publishintern', 'publishcmfi', 'forcepublish', 'publishconfidential'))
    sdef.setPermission('Access contents information', 1, ['Manager', 'Owner', 'Reviewer'])
    sdef.setPermission('Modify portal content', 0, ['Manager', 'Reviewer'])
    sdef.setPermission('View', 1, ['Manager', 'Owner', 'Reviewer'])
    sdef.setPermission('View management screens', 1, ['Manager', 'Reviewer'])
    sdef.setPermission('Change portal events', 0, ['Manager'])

    sdef = wf.states['public']
    sdef.setProperties(title="""Internal CMFI Document (public)""",
                       transitions=('retract', ''))
    sdef.setPermission('Access contents information', 0, ['Manager', 'Owner', 'Member', 'Reviewer', 'Authenticated'])
    sdef.setPermission('Modify portal content', 0, ['Manager', 'Reviewer'])
    sdef.setPermission('View', 0, ['Manager', 'Owner', 'Member', 'Reviewer', 'Authenticated'])
    sdef.setPermission('Change portal events', 0, ['Manager'])
  
    sdef = wf.states['publicintern']
    sdef.setProperties(title="""Document is viewable for departement members""",
                       transitions=('retract', ''))
    sdef.setPermission('Access contents information', 0, ['Manager', 'Owner', 'Member', 'Reviewer'])
    sdef.setPermission('Modify portal content', 0, ['Manager', 'Reviewer' ,'Reviewer'])
    sdef.setPermission('View', 0, ['Manager', 'Owner', 'Member', 'Reviewer'])
    sdef.setPermission('Change portal events', 0, ['Manager'])
 
    sdef = wf.states['visible']
    sdef.setProperties(title="""Document is viewable on the Intranet""",
                       transitions=('hide', 'submit', 'publishcmfi', 'forcepublish', 'publishintern', 'publishconfidential'))
    sdef.setPermission('Access contents information', 1, ['Member', 'Manager', 'Owner'])
    sdef.setPermission('Modify portal content', 1, ['Member', 'Manager', 'Owner'])
    sdef.setPermission('View', 1, ['Reviewer', 'Member', 'Manager', 'Owner'])
    sdef.setPermission('Change portal events', 0, ['Manager'])

    sdef = wf.states['publicconfidential']
    sdef.setProperties(title="""Only cader can access this Document""",
                       transitions=('retract', ''))
    sdef.setPermission('Access contents information', 0, ['Manager', 'Manager', 'Reviewer'])
    sdef.setPermission('Modify portal content', 0, ['Manager', 'Reviewer', 'Reviewer'])
    sdef.setPermission('View', 0, ['Manager', 'Manager', 'Reviewer'])
    sdef.setPermission('Change portal events', 0, ['Manager'])
  
    # not needed yet!
    #sdef = wf.states['publicextranet']
    #sdef.setProperties(title="""Document is viewable on the Internet""",
    #                   transitions=('internalize', ''))
    #sdef.setPermission('Access contents information', 0, ['Manager', 'Owner' ,'Member', 'Anonymous', 'Reviewer'])
    #sdef.setPermission('Modify portal content', 0, ['Manager', 'Reviewer' ,'Reviewer'])
    #sdef.setPermission('View', 0, ['Manager', 'Owner', 'Member', 'Anonymous', 'Reviewer'])
    #sdef.setPermission('Change portal events', 0, ['Manager'])
  
    ## Transitions initialization
    
    tdef = wf.transitions['hide']
    tdef.setProperties(title="""Make Document private""",
                       new_state_id="""private""",
                       trigger_type=1,
                       script_name="""""",
                       after_script_name="""takeOwnershipIfPublished""",
                       actbox_name="""hide""",
                       actbox_url="""%(content_url)s/content_hide_form""",
                       actbox_category="""workflow""",
                       props={'guard_roles': 'Owner'},
                       )

    #tdef = wf.transitions['internalize']
    #tdef.setProperties(title="""Revoke Document from the Internet""",
    #                   new_state_id="""publishedintern""",
    #                   trigger_type=1,
    #                   script_name="""""",
    #                   after_script_name="""moveOnPublish""",
    #                   actbox_name="""retract from internet""",
    #                   actbox_url="""%(content_url)s/content_hide_form""",
    #                   actbox_category="""workflow""",
    #                   props={'guard_permissions': 'Review portal content'},
    #                   )

    #tdef = wf.transitions['externalize']
    #tdef.setProperties(title="""Publish Document on the Internet""",
    #                   new_state_id="""publishedexternal""",
    #                   trigger_type=1,
    #                   script_name="""""",
    #                   after_script_name="""moveOnPublish""",
    #                   actbox_name="""publish on Internet""",
    #                   actbox_url="""%(content_url)s/content_hide_form""",
    #                   actbox_category="""workflow""",
    #                   props={'guard_permissions': 'Review portal content'},
    #                   )

    tdef = wf.transitions['publishconfidential']
    tdef.setProperties(title="""Make Document confidental departement internally""",
                       new_state_id="""publicconfidential""",
                       trigger_type=1,
                       script_name="""""",
                       after_script_name="""moveOnPublish""",
                       actbox_name="""Team management confidental""",
                       actbox_url="""%(content_url)s/content_hide_form""",
                       actbox_category="""workflow""",
                       props={'guard_permissions': 'Review portal content'},
                       )

    tdef = wf.transitions['publishcmfi']
    tdef.setProperties(title="""Make Document public CMFI internally""",
                       new_state_id="""public""",
                       trigger_type=1,
                       script_name="""""",
                       after_script_name="""moveOnPublish""",
                       actbox_name="""CMFI public""",
                       actbox_url="""%(content_url)s/content_hide_form""",
                       actbox_category="""workflow""",
                       props={'guard_permissions': 'Review portal content'},
                       )

    # this is a fake transition used for publishing items
    # from inside scripts without being checked by workflow scripts
    tdef = wf.transitions['forcepublish']
    tdef.setProperties(title="""Make Document public CMFI internally""",
                       new_state_id="""public""",
                       trigger_type=1,
                       script_name="""""",
                       after_script_name="""""",
                       actbox_name="""""",
                       actbox_url="""%(content_url)s/content_hide_form""",
                       actbox_category="""workflow""",
                       props={'guard_permissions': 'Review portal content', 'guard_roles': 'Manager'},
                       )

    tdef = wf.transitions['publishintern']
    tdef.setProperties(title="""Make Document public departement wide""",
                       new_state_id="""publicintern""",
                       trigger_type=1,
                       script_name="""""",
                       after_script_name="""moveOnPublish""",
                       actbox_name="""Team public""",
                       actbox_url="""%(content_url)s/content_hide_form""",
                       actbox_category="""workflow""",
                       props={'guard_permissions': 'Review portal content'},
                       )

    tdef = wf.transitions['reject']
    tdef.setProperties(title="""Reject Document for revision""",
                       new_state_id="""visible""",
                       trigger_type=1,
                       script_name="""""",
                       after_script_name="""notifyOnReject""",
                       actbox_name="""reject""",
                       actbox_url="""%(content_url)s/content_hide_form""",
                       actbox_category="""workflow""",
                       props={'guard_permissions': 'Review portal content'},
                       )

    tdef = wf.transitions['retract']
    tdef.setProperties(title="""Retract Document""",
                       new_state_id="""visible""",
                       trigger_type=1,
                       script_name="""""",
                       after_script_name="""takeOwnershipIfPublished""",
                       actbox_name="""retract""",
                       actbox_url="""%(content_url)s/content_hide_form""",
                       actbox_category="""workflow""",
                       props={'guard_permissions': 'Request review', 'guard_roles': 'Owner'},
                       )

    tdef = wf.transitions['submit']
    tdef.setProperties(title="""Request review of item""",
                       new_state_id="""pending""",
                       trigger_type=1,
                       script_name="""setTransitionRoles""",
                       after_script_name="""""",
                       actbox_name="""submit""",
                       actbox_url="""%(content_url)s/content_hide_form""",
                       actbox_category="""workflow""",
                       props={'guard_permissions': 'Request review'},
                       )
    
    tdef = wf.transitions['show']
    tdef.setProperties(title="""Make visible, do not publish""",
                       new_state_id="""visible""",
                       trigger_type=1,
                       script_name="""""",
                       after_script_name="""""",
                       actbox_name="""show""",
                       actbox_url="""%(content_url)s/content_show_form""",
                       actbox_category="""workflow""",
                       props={'guard_roles': 'Owner'},
		       ) 

    ## State Variable
    wf.variables.setStateVar('review_state')

    ## Variables initialization
    vdef = wf.variables['action']
    vdef.setProperties(description="""The last transition""",
                       default_value="""""",
                       default_expr="""transition/getId|nothing""",
                       for_catalog=0,
                       for_status=1,
                       update_always=1,
                       props=None)

    vdef = wf.variables['review_history']
    vdef.setProperties(description="""Provides access to workflow history""",
                       default_value="""""",
                       default_expr="""state_change/getHistory""",
                       for_catalog=0,
                       for_status=0,
                       update_always=0,
                       props={'guard_permissions': 'Request review; Review portal content'})

    vdef = wf.variables['comments']
    vdef.setProperties(description="""Comments about the last transition""",
                       default_value="""""",
                       default_expr="""python:state_change.kwargs.get('comment', '')""",
                       for_catalog=0,
                       for_status=1,
                       update_always=1,
                       props=None)

    vdef = wf.variables['actor']
    vdef.setProperties(description="""The ID of the user who performed the last transition""",
                       default_value="""""",
                       default_expr="""user/getId""",
                       for_catalog=0,
                       for_status=1,
                       update_always=1,
                       props=None)

    vdef = wf.variables['time']
    vdef.setProperties(description="""Reviewer tasks""",
                       default_value="""""",
                       default_expr="""state_change/getDateTime""",
                       for_catalog=0,
                       for_status=1,
                       update_always=1,
                       props=None)

    ## Worklists Initialization
    ldef = wf.worklists['reviewer_queue']
    ldef.setProperties(description='Reviewer tasks',
                       actbox_name='Pending (%(count)d)',
                       actbox_url='%(portal_url)s/search?review_state=pending',
                       props={'var_match_review_state':'pending',
                              'guard_permissions':p_review})
    

def createCMFI_workflow(id):
    "..."
    ob = DCWorkflowDefinition(id)
    setupCMFI_workflow(ob)
    return ob

addWorkflowFactory(createCMFI_workflow,
                   id='cmfi_workflow',
                   title='Default Workflow [CMFIntranet]')
