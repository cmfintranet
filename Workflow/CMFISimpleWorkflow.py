# -*- coding: UTF-8 -*-
# -*- Mode: Python; py-indent-offset: 4 -*-
# Authors:  Reto Buerki <reto.buerki (at) fhso.ch> 
#
# $Id$
# $HeadURL$

from Products.CMFCore.WorkflowTool import addWorkflowFactory
from Products.CMFCore.CMFCorePermissions import ReviewPortalContent
from Products.DCWorkflow.DCWorkflow import DCWorkflowDefinition

p_review = ReviewPortalContent

def setupCMFISimple_workflow(wf):
    "..."
    wf.setProperties(title='Simple Workflow [CMFIntranet]')

    for s in ['private', 'pending', 'public', 'rejected']:
        wf.states.addState(s)
    for t in ['hide', 'publishcmfi', 'reject', 'retract', 'submit']:
        wf.transitions.addTransition(t)
    for v in ['action', 'actor', 'comments', 'review_history', 'time']:
        wf.variables.addVariable(v)
    for l in ('reviewer_queue',):
        wf.worklists.addWorklist(l)
    for p in ('Access contents information', 'Modify portal content', 'View', 'View management screens'):
        wf.addManagedPermission(p)
        

    ## Initial State
    wf.states.setInitialState('private')

    ## States initialization
    sdef = wf.states['private']
    sdef.setProperties(title="""Visible and editable only by owner""",
                       transitions=('submit', 'publishcmfi'))
    sdef.setPermission('Access contents information', 0, ['Manager', 'Owner'])
    sdef.setPermission('Modify portal content', 0, ['Manager', 'Owner'])
    sdef.setPermission('View', 0, ['Manager', 'Owner'])
    sdef.setPermission('Change portal events', 0, ['Manager'])
    
    sdef = wf.states['pending']
    sdef.setProperties(title="""Waiting for reviewer""",
                       transitions=('retract', 'reject', 'publishcmfi'))
    sdef.setPermission('Access contents information', 1, ['Manager', 'Owner', 'Reviewer'])
    sdef.setPermission('Modify portal content', 0, ['Manager', 'Reviewer'])
    sdef.setPermission('View', 1, ['Manager', 'Owner', 'Reviewer'])
    sdef.setPermission('View management screens', 1, ['Manager', 'Reviewer'])
    sdef.setPermission('Change portal events', 0, ['Manager'])

    ## this one is needed to grant view rights to reviewers (not like private)
    sdef = wf.states['rejected']
    sdef.setProperties(title="""Waiting again for reviewer""",
                       transitions=('submit', 'retract'))
    sdef.setPermission('Access contents information', 0, ['Manager', 'Owner'])
    sdef.setPermission('Modify portal content', 0, ['Manager', 'Owner'])
    sdef.setPermission('View', 1, ['Manager', 'Owner', 'Reviewer'])
    sdef.setPermission('Change portal events', 0, ['Manager'])


    sdef = wf.states['public']
    sdef.setProperties(title="""Internal CMFI Document (public)""",
                       transitions=('retract', ''))
    sdef.setPermission('Access contents information', 0, ['Manager', 'Owner', 'Member', 'Reviewer', 'Authenticated'])
    sdef.setPermission('Modify portal content', 0, ['Manager', 'Reviewer'])
    sdef.setPermission('View', 0, ['Manager', 'Owner', 'Member', 'Reviewer', 'Authenticated'])
    sdef.setPermission('Change portal events', 0, ['Manager'])        
  
    ## Transitions initialization
    
    tdef = wf.transitions['hide']
    tdef.setProperties(title="""Make Document private""",
                       new_state_id="""private""",
                       trigger_type=1,
                       script_name="""""",
                       after_script_name="""""",
                       actbox_name="""hide""",
                       actbox_url="""%(content_url)s/content_hide_form""",
                       actbox_category="""workflow""",
                       props={'guard_roles': 'Owner'},
                       )      

    tdef = wf.transitions['publishcmfi']
    tdef.setProperties(title="""Make Document public CMFI internally""",
                       new_state_id="""public""",
                       trigger_type=1,
                       script_name="""""",
                       after_script_name="""""",
                       actbox_name="""CMFI public""",
                       actbox_url="""%(content_url)s/content_hide_form""",
                       actbox_category="""workflow""",
                       props={'guard_permissions': 'Review portal content'},
                       )   

    tdef = wf.transitions['reject']
    tdef.setProperties(title="""Reject Document for revision""",
                       new_state_id="""rejected""",
                       trigger_type=1,
                       script_name="""""",
                       after_script_name="""notifyOnReject""",
                       actbox_name="""reject""",
                       actbox_url="""%(content_url)s/content_hide_form""",
                       actbox_category="""workflow""",
                       props={'guard_permissions': 'Review portal content'},
                       )

    tdef = wf.transitions['retract']
    tdef.setProperties(title="""Retract Document""",
                       new_state_id="""private""",
                       trigger_type=1,
                       script_name="""""",
                       after_script_name="""""",
                       actbox_name="""retract""",
                       actbox_url="""%(content_url)s/content_hide_form""",
                       actbox_category="""workflow""",
                       props={'guard_permissions': 'Request review', 'guard_roles': 'Owner'},
                       )     
   
    tdef = wf.transitions['submit']
    tdef.setProperties(title="""Request review of item""",
                       new_state_id="""pending""",
                       trigger_type=1,
                       script_name="""setTransitionRoles""",
                       after_script_name="""""",
                       actbox_name="""submit""",
                       actbox_url="""%(content_url)s/content_hide_form""",
                       actbox_category="""workflow""",
                       props={'guard_permissions': 'Request review'},
                       )


    ## State Variable
    wf.variables.setStateVar('review_state')

    ## Variables initialization
    vdef = wf.variables['action']
    vdef.setProperties(description="""The last transition""",
                       default_value="""""",
                       default_expr="""transition/getId|nothing""",
                       for_catalog=0,
                       for_status=1,
                       update_always=1,
                       props=None)

    vdef = wf.variables['review_history']
    vdef.setProperties(description="""Provides access to workflow history""",
                       default_value="""""",
                       default_expr="""state_change/getHistory""",
                       for_catalog=0,
                       for_status=0,
                       update_always=0,
                       props={'guard_permissions': 'Request review; Review portal content'})

    vdef = wf.variables['comments']
    vdef.setProperties(description="""Comments about the last transition""",
                       default_value="""""",
                       default_expr="""python:state_change.kwargs.get('comment', '')""",
                       for_catalog=0,
                       for_status=1,
                       update_always=1,
                       props=None)

    vdef = wf.variables['actor']
    vdef.setProperties(description="""The ID of the user who performed the last transition""",
                       default_value="""""",
                       default_expr="""user/getId""",
                       for_catalog=0,
                       for_status=1,
                       update_always=1,
                       props=None)

    vdef = wf.variables['time']
    vdef.setProperties(description="""Reviewer tasks""",
                       default_value="""""",
                       default_expr="""state_change/getDateTime""",
                       for_catalog=0,
                       for_status=1,
                       update_always=1,
                       props=None)

    ## Worklists Initialization
    ldef = wf.worklists['reviewer_queue']
    ldef.setProperties(description='Reviewer tasks',
                       actbox_name='Pending (%(count)d)',
                       actbox_url='%(portal_url)s/search?review_state=pending',
                       props={'var_match_review_state':'pending',
                              'guard_permissions':p_review})
    

def createCMFISimple_workflow(id):
    "..."
    ob = DCWorkflowDefinition(id)
    setupCMFISimple_workflow(ob)
    return ob

addWorkflowFactory(createCMFISimple_workflow,
                   id='cmfi_simple_workflow',
                   title='Simple Workflow [CMFIntranet]')
