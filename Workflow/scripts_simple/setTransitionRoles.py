## Script (Python) "setTransitionRoles"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=state_change
##title=set correct local roles

if context.meta_type == "CMF CMFIProfile":
    obj = getattr(state_change, 'object')
   
    reviewers = context.portal_cmfi_tool.getItemFromGP('cmfi_cmfiprofile_reviewer')
    for reviewer in reviewers:
        g = context.portal_groups.getGroupById(reviewer)
        obj.manage_setLocalRoles(g.getId(), ['Reviewer'])  
