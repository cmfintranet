# -*- coding: UTF-8 -*-
# -*- Mode: Python; py-indent-offset: 4 -*-
# Authors:  Reto Buerki <reto.buerki (at) fhso.ch>
#
# $Id$
# $HeadURL$

from Products.CMFCore.utils import getToolByName
from Globals import package_home
from Products.PythonScripts.PythonScript import manage_addPythonScript

from os import path, listdir

def add_workflow(self, wf_id, wf_factory_id, out):	
    """ 
    Add a workflow definition to the portal_workflow tool
    """
    wf_tool = self.portal_workflow
    try:
        wf_tool.manage_delObjects([wf_id])
        out.write('[*] Removed existant workflow "%s"\n'%wf_id)
    except:
        pass
    wf_tool.manage_addWorkflow(wf_factory_id, wf_id)
    
    out.write('[*] Added workflow "%s"\n'%wf_id)

def install_workflow(portal, out):
    """
    Install a workflow to portal
    """
    import CMFIWorkflow
    reload(CMFIWorkflow)
    
    wf_tool = getToolByName(portal, 'portal_workflow')
    # install defaul workflow
    add_workflow(portal, 'cmfi_workflow', 'cmfi_workflow (Default Workflow [CMFIntranet])', out)
    # install simple workflow
    add_workflow(portal, 'cmfi_simple_workflow', 'cmfi_simple_workflow (Simple Workflow [CMFIntranet])', out)
    install_scripts(portal, 'cmfi_workflow', out)  
    install_scripts(portal, 'cmfi_simple_workflow', out, "yes")

def install_scripts(portal, wf_id, out, simple=None):
    """
    Install scripts in the workflows scriptfolder - dumpDCWorkflow.py
    doesn't export scripts, so we need to add them explicitly
    """
    wf_tool = getToolByName(portal, 'portal_workflow')
    wf = getattr(wf_tool, wf_id)
    script_folder = wf.scripts

    if simple == "yes":
        fs_script_path = path.join(package_home(globals()), 'scripts_simple')
    else:
        fs_script_path = path.join(package_home(globals()), 'scripts')
    for file in listdir(fs_script_path):
        if '.' in file:
            (name, ext) = file.split('.', 1)
            if ext == 'py':
                print >> out, "[*] Installing wf script %s/%s" % (fs_script_path, file)
                loadPy(script_folder, "%s/%s" % (fs_script_path, file), name) 

def loadPy(script_folder, path, name):
    """
    Install a script and set proxy to manager
    """
    f = open(path, "r")
    data = f.read()
    f.close()

    if not hasattr(script_folder, name):
        manage_addPythonScript(script_folder, name)
    o = script_folder._getOb(name)
    o.write(data)
    o.manage_proxy(['Manager'])
