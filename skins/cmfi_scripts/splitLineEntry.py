## Script (Python) "splitLineEntry"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=line=[]
##title=splits a line delimited by ';'
##
try:
    return line.split(';')
except:
    return ''

