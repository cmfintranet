## Script (Python) "loginHook"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=
##title=Hook to login, checks if CMFIProfile exists for given user
##

time = str(context.ZopeTime())
# get user
user = context.portal_membership.getAuthenticatedMember()

# check if user has a CMFIProfile
results = context.portal_catalog(meta_type = 'CMF CMFIProfile', review_state = ('public','pending'), userid = user.id )

portal_url = context.portal_url.getPortalPath()
RESPONSE = context.REQUEST.RESPONSE

if (len(results) > 0):
  # CMFIProfile found or no profile needed, go to portal
  url = '%s/%s'
  action = 'news' 
  # update login times
  login_time = user.getProperty('login_time', DateTime())
  user.setProperties(last_login_time=login_time);
  user.setProperties(login_time=context.ZopeTime())
    
else: 
  # prepare the redirection
  url = '%s/%s'
  action = 'Members/'+user.id+'/index_html/portal_form/cmfi_profile_edit_form'

# if user not authenticated
if user.id == 'acl_users':
  url = '%s/%s'
  action = 'login_form?retry=1&disable_cookie_login__=1'

return RESPONSE.redirect( url % ( portal_url
                                    , action ))
