## Script (Python) "change_cmfi_properties"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=proplist=None
##title=Change CMFIntranet Properties
##

request = container.REQUEST
RESPONSE =  request.RESPONSE

for x in request.form.keys():
    request.set(x,request.form.get(x))

context.portal_properties.cmfi_properties.manage_changeProperties(request)

msg = 'Successfully changed Intranet settings!'

return RESPONSE.redirect("%s/prefs_cmfi_props?portal_status_message=%s" \
							% (context.portal_url(), msg))	
