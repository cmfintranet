## Script (Python) "manage_site_structure"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=button_action=None, new_structure=None, main_struct_tree=None
##title=Change CMFIntranet site structure
##
request = context.REQUEST

conf_tool = getattr(context, 'portal_cmfi_config_tool')

success = 0

if button_action == "add":
    # add new division
    if new_structure  == "": return
    if conf_tool.add_main_struct(new_structure): success = 1
if button_action == "delete":
    if main_struct_tree == None: return
    if conf_tool.del_main_struct(main_struct_tree[0]): success = 1
    pass

if success == 1: msg = 'Site structure successfully updated!'
else : msg = 'Could not update site structure!'

return request.RESPONSE.redirect("%s/prefs_cmfi_management?portal_status_message=%s" \
						% (context.portal_url(), msg))	
