## Script (Python) "checkForFileTypes"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=process=None
##title=check if file objects of type defined in cmfi.properties are found and returns the result as a list
##

from zLOG import LOG,INFO
request = context.REQUEST

result=[]

# read in the file types defined in the property file
filetypes = getattr(context, 'portal_cmfi_tool').getItemFromGP('cmfi_file_types')

catalog = context.portal_catalog
# search for the portal files
for type in filetypes:
  key = type[:type.find(':')]
  thissubject = type[type.find('/')+1:]
  
  entries = catalog.searchResults( Subject = ('education', 'wording')
                                 , Subject_operator = 'and'
                                 , review_state = 'public' );
  
  # add string of format '[len];[subject]' to dictionnary  
  if len(entries) > 0:  
    result.append(key+';'+str(len(entries))+';'+thissubject)

return result
