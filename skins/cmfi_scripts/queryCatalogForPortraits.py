## Script (Python) "queryCatalogForPortraits"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=
##title=wraps the portal_catalog with a rules qualified query
##
results=[]
REQUEST=context.REQUEST
catalog=context.portal_catalog
indexes=catalog.indexes()
query={}

for i in indexes:
    v=REQUEST.get(i, None)
    if v:
        query.update({i:v})

notRawIndexFields=[k for k in REQUEST.form.keys() if k not in query.keys()]
if notRawIndexFields:
    for k in notRawIndexFields:
        if k.endswith('_usage'): 
            query.update({k:REQUEST.get(k)})

# limit query to CMFIProfile (NOT CMFIAdmPhone because this class does not have a key 'userid')
meta_info = {'meta_type':['CMF CMFIProfile']}
query.update(meta_info)

# sort by userid
sort_info = {'sort_on':'userid'}
query.update(sort_info)

# limit to public objects
review_info = {'review_state':'public'}
query.update(review_info)

if query:
    results=catalog(query)
    
# if floor attribute is set skip those portraits which are not in the given range
tmp_portraits = []
floor = REQUEST.get('floor',None)
if floor:
    for cmfportrait in results:
        p = cmfportrait.getObject()
        if p.roomnr and (p.roomnr[0] == floor):
            tmp_portraits.append(cmfportrait)            
    #if len(tmp_portraits)>0:
    results = tmp_portraits
 
return results

