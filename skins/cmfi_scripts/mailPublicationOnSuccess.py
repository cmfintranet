## Script (Python) "mailPublicationOnSuccess"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=reviewer='',receiver='',title='',targetfolder=''
##title=Mail a notification to the submitter that publication transition was successful
##

from zLOG import LOG, INFO

try:
  email_to = context.portal_cmfi_tool.getEmailFromMemberdata(receiver)
  email_from = context.portal_cmfi_tool.getEmailFromMemberdata(reviewer)
  if len(email_to)>0 and len(email_from)>0:
    # get the mailtext placing the variables into a template
    mailtext = context.mail_publication_success_template(context, 
                                                     sender=email_from,
                                                     receiver=email_to,
                                                     title=title,
                                                     targetfolder=targetfolder,)
    
    # get the mailhost defined for this site
    mailhost = context.MailHost

    # send the mail
    mailhost.send( mailtext )
    LOG('CMFIntranet.mailPublicationOnSuccess:', INFO, 'email with title [' + title + '] sent from ' + email_from + ' to ' + email_to)
except:
    LOG('CMFIntranet.mailPublicationOnSuccess:', INFO, 'email with title [' + title + '] could not be sent from ' +reviewer + ' to ' + receiver)
    raise Exception( 'WARNING: Mail Message could not be sent!' )
