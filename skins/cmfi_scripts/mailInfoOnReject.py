## Script (Python) "mailInfoOnReject"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=reviewer='',submitter='',title=''
##title=Mail an info to the submitter that publication is rejected
##

from zLOG import LOG, INFO

try:
  # get the review list and the comment
  review_history = context.portal_workflow.getInfoFor(context, 'review_history')
  last_comment = review_history[len(review_history)-1]

  email_to = context.portal_cmfi_tool.getEmailFromMemberdata(submitter)
  email_from = context.portal_cmfi_tool.getEmailFromMemberdata(reviewer)
  LOG('CMFIntranet.mailInfoOnReject:', INFO, 'email with title [' + title + '] trying to sent from ' + email_from + ' to ' + email_to)
  if len(email_to)>0 and len(email_from)>0:
    # get the mailtext placing the variables into a template
    mailtext = context.mail_reject_template(context, 
                                            sender=email_from,
                                            receiver=email_to,
                                            title=title,
                                            comment=last_comment['comments'] )
    
    # get the mailhost defined for this site
    mailhost = context.MailHost

    # send the mail
    mailhost.send( mailtext )
    LOG('CMFIntranet.mailInfoOnReject:', INFO, 'email with title [' + title + '] sent from ' + email_from + ' to ' + email_to)
except:
    LOG('CMFIntranet.mailInfoOnReject:', INFO, 'email with title [' + title + '] could not be sent')
    raise Exception( 'WARNING: Mail Message could not be sent!' )
