## Script (Python) "manage_cmfi_list_properties"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=button=None,gprop_name=None,proplist=None,prop=None
##title=Manages the cmfi list properties
##
request = context.REQUEST

conf_tool = getattr(context, 'portal_cmfi_config_tool')

success = 0

if prop == '':
    if proplist == None: return
    if conf_tool.delete_global_prop_items(proplist, gprop_name): success = 1    
elif proplist == None:
    if prop == '': return
    if conf_tool.add_global_prop_items(prop, gprop_name): success = 1  
							 
if success == 1: msg = 'Global Property successfully updated!'
else : msg = 'Could not update Global Property!'

return request.RESPONSE.redirect("%s/prefs_cmfi_props?portal_status_message=%s" \
						% (context.portal_url(), msg))	
