# Redirect to the Login Form if the user is anonymous

request = container.REQUEST
RESPONSE =  request.RESPONSE  

isAnonymous = context.portal_membership.isAnonymousUser()
pageloc = request.URL.split('/')[-1]
okToShowHere = pageloc in ('login_form') or pageloc in ('logged_in') or pageloc in ('logged_out')

if isAnonymous and (not okToShowHere):
    REQUEST=context.REQUEST
    REQUEST.RESPONSE.redirect(str(context.portal_url()))
return True
