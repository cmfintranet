## Script (Python) "listToPDF"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=
##title=returns given telephone list as pdf stream

REQUEST=context.REQUEST
catalog=context.portal_catalog
indexes=catalog.indexes()
queryString=[]
for i in indexes:
    v=REQUEST.get(i, None)
    if v: queryString.append(v)

items=context.queryCatalogForPortraits();

profiles = []
for item in items:
  profiles.append(item.getObject())
  
content = None
try:
  content = context.portal_cmfi_pmm_tool.telephoneListAsPDF(queryString, profiles)
  context.REQUEST.RESPONSE.setHeader('Content-Type', 'application/pdf')
except:
  pass

return content
