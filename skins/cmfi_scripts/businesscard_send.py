## Script (Python) "businesscard_send"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=name="",edit="",send="",firstname="",division="",salutation="",acad_title="",role="",telephone="",telefax="",email=""
##title=Send a CMFI BusinessCard
##

if (edit == "edit" or edit == "bearbeiten") and not send:
  portal_url = context.portal_url()
  RESPONSE=context.REQUEST.RESPONSE
  url = '%s/%s?%s'
  msg = 'name=%s&firstname=%s&division=%s&salutation=%s&acad_title=%s&role=%s&telephone=%s&telefax=%s&email=%s' % \
      (name,firstname,division,salutation,acad_title,role,telephone,telefax,email)
  return RESPONSE.redirect( url % ( portal_url
                                    , 'businesscard_edit_form'
                                    , msg ) )

else:
  tool = context.portal_cmfi_tool
  address = tool.getAddress(division)
  phone = tool.getItemFromGP('cmfi_country_code') + '  ' + '(0)' + telephone[1:]
  fax = tool.getItemFromGP('cmfi_country_code') + '  ' + '(0)' + telefax[1:]
  url = tool.getItemFromGP('cmfi_url')
  tel = tool.getItemFromGP('cmfi_tel_company')

  # get the serivce which will deal with this new request
  service = tool.getItemFromGP('cmfi_businesscard_service_email')

  # get the mailtext placing the variables into a template
  mailtext = context.mail_businesscard_template(context, 
                                           name=name,
                                           firstname=firstname,
                                           address=address,
                                           role=role,
                                           salutation=salutation,
                                           acad_title = acad_title,
                                           phone=phone,
                                           fax=fax,
                                           email=email,
                                           url=url,
                                           tel=tel,
                                           service=service,
                                           )
  # get the mailhost defined in for this site
  mailhost = context.MailHost
  
  # send the mail
  mailhost.send( mailtext )

  # prepare an answer
  portal_url=context.portal_url()
  RESPONSE=context.REQUEST.RESPONSE
  url = '%s/%s?%s'  
  msg = 'msg=Ihre Bestellung wurde an %s zur Weiterbearbeitung gesandt!' % service
  return RESPONSE.redirect( url % ( portal_url
                                    , 'businesscard_success'
                                    , msg ) )

