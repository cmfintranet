## Script (Python) "upload_logo"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=file=None,upload_type=None
##title=Change CMFIntranet Logos
##

request = context.REQUEST

conf_tool = getattr(context, 'portal_cmfi_config_tool')

if conf_tool.change_logo(file, upload_type):
    upload = int(upload_type)
    if upload == 0:
        msg = 'Successfully changed CMFIntranet Logo! Please refresh your browser if new image is not yet visible...'
    elif upload == 1:
        msg = 'Successfully changed CMFIntranet Logo! You may log out to verify...'
    else:
        msg = 'Received an unknown upload_type. This should not happen...'        

else:
    msg = 'Could not upload Image!'

return request.RESPONSE.redirect("%s/prefs_cmfi_form?portal_status_message=%s" \
							% (context.portal_url(), msg))	
