## Script (Python) "getFilteredSubjects"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=subjects=None,sitemap=None
##title=filter the subjects, exclude the sitemap subjects
##

selection = []
cols= ['cms-home']  # filter cms-home as top-level

for siteElement in sitemap:
  cols.append(siteElement[1])
  
for subject in subjects:
  if not subject in cols:
    selection.append(subject)
 
return selection