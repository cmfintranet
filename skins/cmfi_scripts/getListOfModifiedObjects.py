## Script (Python) "getListOfModifiedObjects"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=
##title=returns a list of all modified objects starting with the last modified
##

results = context.portal_catalog (sort_order='reverse',sort_on='modified',review_state='public')

list = []

if len(results)>0:
    for i in range(30):
        if i<len(results):
            result = results[i]
            obj = result.getObject()
            row = {}
            if ( obj != None ):
                row['creator'] = result.Creator
                row['url'] = result.getURL()
                row['path'] = result.getPath()
                row['time'] = result.modified
                list.append( row )
        else:
            pass
        
return list

#catalog = context.portal_catalog

#list = catalog(bobobase_modification_time=DateTime()-10,
     #bobobase_modification_time_usage='range:min',
     #review_state='public',
     #sort_on='modified',
     #sort_order='reverse')
 
#return list
