## Script (Python) "queryCatalogForEvents"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=
##title=wraps the portal_catalog with a rules qualified query
##
results=[]
REQUEST=context.REQUEST
catalog=context.portal_catalog
indexes=catalog.indexes()
query={}

for i in indexes:
    v=REQUEST.get(i, None)
    if v:
        query.update({i:v})

notRawIndexFields=[k for k in REQUEST.form.keys() if k not in query.keys()]
if notRawIndexFields:
    for k in notRawIndexFields:
        if k.endswith('_usage'): 
            query.update({k:REQUEST.get(k)})

meta_info = {'meta_type':['CMF Event']}
query.update(meta_info)

# sort by userid
sort_info = {'sort_on':'start'}
query.update(sort_info)

# sort by userid
#sort_order = {'sort_order':'reverse'}
#query.update(sort_order)

# limit to public objects
review_info = {'review_state':'public'}
query.update(review_info)

# limit to non-expired objects
#review_info = {'expired':'false'}
#query.update(review_info)

if query:
    results=catalog(query)

tmp_events=[]
eventtype = REQUEST.get('eventtype',None)

if eventtype:
    for event in results:
        p = event.getObject()
        if eventtype in event.Subject:
            tmp_events.append(event)
    results = tmp_events
    
return results

