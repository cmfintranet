## Script (Python) "mailToSelectedUsers"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=
##title=returns a mailto hyperlink with selected users as email receivers

# get items from REQUEST string passed to this script by the caller
items=context.queryCatalogForPortraits();

emails =  ""
for item in items:
  emails = item.getObject().email + ";" + emails

if (len(emails) > 0): 
  url = '%s:%s'
  return context.REQUEST.RESPONSE.redirect( url % ( 'mailto', emails))
else:
  return None


