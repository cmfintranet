## Script (Python) "my_worklist"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=
##title=
##

if context.portal_membership.isAnonymousUser():
    return []
user = getattr(context, 'portal_membership').getAuthenticatedMember()
layoutDic = getattr(context, 'portal_cmfi_tool').getSiteLayout()
checkPermission=context.portal_membership.checkPermission
wf_wlist_map = context.portal_workflow.getWorklists()
catalog=context.portal_catalog
unique_catalog_vars = {}# dict with var:uniqueValuesFor
avail_objs = {} # absolute_url:obj - enables easier filtering

data = {}
for key, value in layoutDic.items():
  if value:
    data[key.split('/')[1]] = map(lambda x: x.split('/')[1], value.split(','))
  else:
    data[key.split('/')[1]] = None

for wlist_map_sequence in wf_wlist_map.values():
    for wlist_map in wlist_map_sequence:
        permissions=wlist_map['guard_permissions']
        roles=wlist_map['guard_roles']
        catalog_vars=wlist_map['catalog_vars']
        types=wlist_map['types']
        # Filter out if we already know there is nothing in the catalog
        skip = 0
        for key,value in catalog_vars.items():
            if not unique_catalog_vars.has_key(key):
                unique_catalog_vars[key] = catalog.uniqueValuesFor(key)
            if not [val for val in value if val in unique_catalog_vars[key]]:
                # Nothing in the catalog, skip to next worklist
                skip = 1
                continue

        # Make sure we have types using this workflow/worklist
        if not skip and types:
            for result in catalog.searchResults(catalog_vars, portal_type=types):
                o = result.getObject()
                absurl = o.absolute_url()
                subjects = o.Subject()

                division_r = None
                process_r = None
                    
                #split layout into its parts
                foundEntry = ""
                process =""
          
                for subject in subjects:
                    process = subject[:subject.find('.')]
                    if data.has_key(subject):
                        foundEntry = subject
                        break
  
                    # check now if subject is given in layout column    
                    if data.has_key(process):
                        foundEntry = subject
                        break    

                if 'cms-home' in subjects:                    
                    foundEntry = 'cms-home'
                    
                groupname = ''
                groupname_m = ''
                cmfi_tool = getattr(context, 'portal_cmfi_tool')
                if foundEntry:
                    groupname = 'group_' + subject + '_reviewer'
                    groupname_m = 'group_' + subject + '_manager'
                    
                usergroups = user.getGroups()
                
                if (groupname in usergroups) \
			or (groupname_m in usergroups ) \
			or (division_r in usergroups) \
			or (process_r in usergroups):
	            avail_objs[absurl] = o 
                elif o.meta_type in ['CMF CMFIProfile'] and cmfi_tool.isUserCMFIProfileReviewer(user):
                    avail_objs[absurl] = o                           
                elif o.meta_type in ['CMF CMFIAdmPhone', 'CMF Event'] and cmfi_tool.isUserCMFIHomeReviewer(user):
                    avail_objs[absurl] = o

avail_objs = avail_objs.values()
avail_objs.sort(lambda x, y: cmp(x.modified(), y.modified()))
return avail_objs
