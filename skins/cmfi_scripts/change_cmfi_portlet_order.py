## Script (Python) "change_cmfi_portlet_order"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=proplist=None
##title=Change CMFIntranet Portlet display, sorting
##

request = container.REQUEST
RESPONSE =  request.RESPONSE

cmfi_properties = context.portal_properties.cmfi_properties

portlets = cmfi_properties.cmfi_portlets_available
c= 0

for portlet in portlets:
    portlet = 'cmfi_' + portlet
    cmfi_properties.manage_changeProperties({portlet:proplist[c]})
    c = c + 1

msg = 'Successfully changed Portlet Order settings!'

return RESPONSE.redirect("%s/prefs_cmfi_management?portal_status_message=%s" \
							% (context.portal_url(), msg))	
