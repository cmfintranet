## Script (Python) "getProcessInfo"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=
##title=gets process and divisioninfo out of the subjects
##



# SPEZIAL objects, not related to a specific process
if context.meta_type in ['CMF CMFIProfile','CMF CMFIAdmPhone', 'CMF Event']:
  return ('spezial', 'OK')

ptool = getattr(context, 'portal_cmfi_tool')
layoutDic = ptool.getSiteLayout()
subjects = context.Subject()

# layoutdic must be split into its parts
# each entry is something like
# 'Ausbildung/degree_courses': 'Technik/engineering,Wirtschaft/business,Soziales/socialwork,Technikerschule/technicians',
data = {}
for key, value in layoutDic.items():
  if value:
    data[key.split('/')[1]] = map(lambda x: x.split('/')[1], value.split(','))
  else:
    data[key.split('/')[1]] = None

#split layout into its parts
foundProcess = ""
# we are looking for a process. If one is found we
# try to determine the division
# if no process is found we look whether the document is to be publishe for the
# whole cmfi. This is indicated by a subject 'cms-home'. However only news
# can have this subject
for subject in subjects:
  # check first if subject is a key itself
  if data.has_key(subject):
    foundProcess = subject
    break

  # check now if subject is given in layout column
  key = subject[:subject.find('.')]
  if data.has_key(key):
    foundProcess = subject
    break

if 'cms-home' in subjects:
  foundProcess = 'cms-home'

ok = 'NOT OK!'
if(foundProcess and foundProcess != 'cms-home'):
  ok = 'OK'
else:
  if context.meta_type == "News Item" and foundProcess == 'cms-home':
    ok = 'OK'

return (len(foundProcess) and foundProcess or 'undefined' , ok)
