## Script (Python) "cmfi_admphone_edit"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=id='',title='',telDirect='',place='',description='',text='',text_format=''
##title=Edit a CMFI admPhone Entry
##

# if there is no id specified, keep the current one
if not id:
    id = context.getId()

new_context = context.portal_factory.doCreate(context, id)

new_context.edit(title = title
             , telDirect = telDirect
             , place = place
             , text = text
             , description = description
             , text_format = text_format)

new_context.plone_utils.contentEdit( context
                               , id=id
                               , description=description)

return state.set(context=new_context, portal_status_message='Phone Entry changes saved.')
