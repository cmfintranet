## Script (Python) "cmfi_profile_edit"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=id='',title='',userid='',salutation='',firstname='',name='',token='',address='',divisions=[],roomnr='',acad_title='',telDirect='',telFax='',email='',mobile='',entryDate='',categories=[],role='',public_services=[],services_centraloffice=[],publications='',text='',text_format=''
##title=Edit a CMFI Profile
##

new_context = context.portal_factory.doCreate(context, id)

new_context.edit(title = title
             , userid = userid
             , salutation = salutation
             , acad_title = acad_title
             , firstname = firstname
             , name = name 
             , token = token
             , divisions = divisions
             , address = address
             , telDirect = telDirect
             , telFax = telFax
             , email = email
             , mobile = mobile
             , roomnr = roomnr
             , entryDate = entryDate
             , categories = categories
             , role = role
             , public_services = public_services
             , services_centraloffice = services_centraloffice
             , text = text
             , publications = publications
             , text_format = text_format)

new_context.plone_utils.contentEdit( context
                               , id=id)

return state.set(context=new_context, portal_status_message='Profile changes saved.')
