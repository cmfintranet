## Script (Python) "cmfi_filetopic_edit"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=text_format, text, title='', description='', id=''
##title=Edit a CMFIFile

# if there is no id specified, keep the current one
if not id:
    id = context.getId()

title = id
new_context = context.portal_factory.doCreate(context, id)

new_context.edit( title
                  , description
                  , text
                  , text_format
                  , subjects = id)

new_context.plone_utils.contentEdit( context
                                   , id=id
                                   , description=description )

return state.set(context=new_context, portal_status_message='CMFIFileTopic changes saved.')
