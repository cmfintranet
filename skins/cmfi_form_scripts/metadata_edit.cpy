## Script (Python) "metadata_edit"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##title=Update Content Metadata
##parameters=allowDiscussion=None,title=None,subject=None,sitemap=None,description=None,contributors=None,effective_date=None,expiration_date=None,format=None,language=None,rights=None

if subject != None:
    subject=[s for s in subject if s] 
else:
    subject = []

if len(subject) < 1 and context.meta_type not in ['CMF CMFIFileTopic', 'News Item']: 
    return state.set(status='failure', portal_status_message='You must select minimum one process and topic!')

if sitemap != None:
  for s in sitemap:
    index = s.find('[')
    if index>0:
      subject.append(s[:s.find('[')])
    else:
      subject.append(s)

context.plone_utils.editMetadata(context,
                                 allowDiscussion=allowDiscussion,
                                 title=title,
                                 subject=subject,
                                 description=description,
                                 contributors=contributors,
                                 effective_date=effective_date,
                                 expiration_date=expiration_date,
                                 format=format,
                                 language=language,
                                 rights=rights)

new_context = context.portal_factory.doCreate(context)
return state.set(context=new_context, portal_status_message='Content properties have been saved.')
