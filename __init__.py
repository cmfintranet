# -*- coding: UTF-8 -*-
# -*- Mode: Python; py-indent-offset: 4 -*-
# Authors:  Reto Buerki <reto.buerki (at) fhso.ch>
#
# $Id$
# $HeadURL$

"""Initialize CMFIntranet"""

import Products.CMFCore
from Products.CMFCore import utils, CMFCorePermissions
from Products.CMFTopic import TopicPermissions
from Products.CMFCore.DirectoryView import registerDirectory
import CMFIProfile, CMFIFileTopic, CMFIAdmPhone
import CMFITool, MembershipTool, ConfigTool, CalendarTool
from Products.CMFPlone import PloneControlPanel
from Workflow import CMFIWorkflow
from Workflow import CMFISimpleWorkflow
import sys

this_module = sys.modules[ __name__ ]

contentConstructors = (CMFIProfile.addCMFIProfile,CMFIFileTopic.addCMFIFileTopic,CMFIAdmPhone.addCMFIAdmPhone,)
contentClasses = (CMFIProfile.CMFIProfile,CMFIFileTopic.CMFIFileTopic,CMFIAdmPhone.CMFIAdmPhone,)
tools = ( CMFITool.CMFITool,
          ConfigTool.ConfigTool,
	  MembershipTool.MembershipTool,
	  CalendarTool.CalendarTool,
	  PloneControlPanel.PloneControlPanel,
	)

cmfi_profile_globals = globals()

z_bases = utils.initializeBasesPhase1(contentClasses, this_module)
z_tool_bases = utils.initializeBasesPhase1( tools, this_module )

# Make the skins available as DirectoryViews
registerDirectory('skins', globals())
registerDirectory('skins/cmfi_content', globals())
registerDirectory('skins/cmfi_forms', globals())
registerDirectory('skins/cmfi_images', globals())
registerDirectory('skins/cmfi_scripts', globals())
registerDirectory('skins/cmfi_form_scripts', globals())
registerDirectory('skins/cmfi_portlets', globals())
registerDirectory('skins/cmfi_templates', globals())

def initialize(context):
    utils.initializeBasesPhase2( z_bases, context )
    utils.initializeBasesPhase2( z_tool_bases, context )
    
    utils.ToolInit( 'CMFI Tool', tools = tools,
                     product_name = 'CMFIntranet', icon='tool.gif',
                    ).initialize( context )
       
    utils.ContentInit('CMF CMFIProfile',
                      content_types = contentClasses,
                      permission = CMFCorePermissions.AddPortalContent,
                      extra_constructors = contentConstructors,
                      fti = CMFIProfile.factory_type_information).initialize(context)
    
    utils.ContentInit('CMF CMFIAdmPhone',
                      content_types = contentClasses,
                      permission = CMFCorePermissions.AddPortalContent,
                      extra_constructors = contentConstructors,
                      fti = CMFIAdmPhone.factory_type_information).initialize(context)    

    utils.ContentInit('CMF CMFIFileTopic',
                      content_types = contentClasses,
                      permission = TopicPermissions.AddTopics,
                      extra_constructors =  (CMFIFileTopic.addCMFIFileTopic,),
                      fti = CMFIFileTopic.factory_type_information).initialize(context)

